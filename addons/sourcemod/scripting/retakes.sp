#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <sdktools>
#include <sdkhooks>
#include <clientprefs>
#include <multicolors>
#include <cstrike>
#include <smlib>

#define PL_VERSION "3.3.0"

public Plugin myinfo = 
{
	name        = "Retakes",
	author		= ".#Zipcore; Credits: Ofir",
	description = "Retake Simulation",
	version     = PL_VERSION,
	url         = "zipcore#googlemail.com"
};

// Enums and Contstants
#define PREFIX "{darkblue}[{orange}RETAKES{darkblue}]{lime} "
#define SITEA 0
#define SITEB 1
#define MAXSPAWNS 64

#define WEAPONTYPE_C4 7

#define FL_USED_Ct (1 << 0)
#define FL_USED_T (1 << 1)

enum SpawnType
{
	None,
	Bomb,
	T,
	Ct
};

enum Spawn
{
	Id,
	SpawnType:Type,
	Float:Location[3],
	Float:Angles[3],
	Site,
	bool:Used
};

//Players Arrays
bool g_bWarnned[MAXPLAYERS+1] = {false, ...};

//Global Vars
bool g_bEditMode = false;
bool g_bScrambling = false;

bool g_bWaitForPlayers = false;

bool g_bBombPlanted = false;

char gs_CurrentMap[64];
int g_iChoosedSite[MAXPLAYERS+1];

int g_iMoneyOffset = -1;
int g_iHelmetOffset = -1;

int g_precacheLaser;
int g_precacheGlow;

float g_fBombPos[3];

float g_fMapLoadTime;
float g_fStartRoundTime;
Handle g_hMapLoadTimer = null;

int g_iWinnerTeam = CS_TEAM_NONE;

int g_iBomb;
int g_iBomber;

int g_iFunMode;

int g_iFlasher = 0;
float g_fFlashDelay;
int g_iTeamflashes;

#define DURATION_LIMIT 2.0
#define ALPHA_LIMIT 255.0

#define VMT_BOMBRING "materials/sprites/bomb_planted_ring.vmt"
#define VMT_HALO "materials/sprites/halo.vmt"

int g_iBombRing, g_iHalo;

int g_iWepType[MAXPLAYERS + 1];

#define RT_FIRST -1

enum
{
	RT_PISTOL = 0,
	RT_FUN,
	RT_FORCE,
	RT_FULL
}

Handle g_hTimerSound = null;

int g_iRoundType;
int g_iSite;
int g_iWinRowCounter;

int g_iAwpCt;
int g_iAwpT;

#include "retakes/stocks.sp"
#include "retakes/cvars.sp"
#include "retakes/game.sp"
#include "retakes/bomb.sp"
#include "retakes/clientsettings.sp"
#include "retakes/gunsmenu.sp"
#include "retakes/teams.sp"
#include "retakes/equip.sp"
#include "retakes/equip_grenades.sp"
#include "retakes/equip_force.sp"
#include "retakes/equip_fun.sp"
#include "retakes/teleport.sp"
#include "retakes/admin.sp"
#include "retakes/flash.sp"
#include "retakes/sql.sp"
#include "retakes/sounds.sp"
#include "retakes/points.sp"
#include "retakes/other.sp"

public void OnPluginStart()
{
	CreateCvars();
	
	RegisterAdminCommands();
	RegisterGunMenuCommands();
	
	RegisterCookies();
	
	ConnectSQL();
	
	AddServerTag("retakes");
	
	HookEvent("round_prestart", Event_OnRoundPreStart);
	HookEvent("round_poststart", Event_OnRoundPostStart);
	HookEvent("round_end", Event_OnRoundEnd);
	HookEvent("player_connect_full", Event_OnFullConnect);
	HookEvent("player_hurt", Event_OnPlayerDamged);
	HookEvent("player_death", Event_Death);
	HookEvent("bomb_defused", Event_OnBombDefused);
	HookEvent("bomb_planted", Event_OnBombPlanted);
	HookEvent("item_equip", Event_OnItemEquip);
	HookEvent("player_blind", Event_PlayerBlind);
	HookEvent("flashbang_detonate", Event_FlashbangDetonate);
	HookEventEx("bomb_beginplant", Event_OnBeginPlant);
	
	HookUserMessage(GetUserMessageId("TextMsg"), MsgHook_TextMsg, true);
	
	AddCommandListener(Hook_ChangeTeamChange, "jointeam");
	
	//Offsets
	g_iMoneyOffset = FindSendPropInfo("CCSPlayer", "m_iAccount");
	g_iHelmetOffset = FindSendPropInfo("CCSPlayer", "m_bHasHelmet");
	
	int iPlayers;
	
	// Late load Support
	LoopIngameClients(iClient)
	{
		if(IsClientSourceTV(iClient))
			continue;
		
		iPlayers++;
		
		// Late load cookies
		LoadClientCoockies(iClient);
		
		if(g_cvQueue.BoolValue && GetClientTeam(iClient) <= CS_TEAM_SPECTATOR)
			AddClientToQueue(iClient);
		
		OnClientPutInServer(iClient);
	}
	
	CreateTimer(0.5, Timer_PrintHintSite, _, TIMER_REPEAT);
}

public void OnClientPutInServer(int iClient) 
{
	SDKHook(iClient, SDKHook_WeaponCanSwitchTo, Hook_WeaponCanSwitch); 
}

public void OnMapStart()
{
	g_iBombRing = PrecacheModel(VMT_BOMBRING);
	g_iHalo = PrecacheModel(VMT_HALO);
	
	g_hTimerSound = null;
	g_hTimerCheckPlayers = null;
	StartPlayerCheckTimer();
	PrepareSounds();
	
	//Map String to LowerCase
	GetCurrentMap(gs_CurrentMap, sizeof(gs_CurrentMap));
	int len = strlen(gs_CurrentMap);
	
	for (int i = 0; i < len; i++)
		gs_CurrentMap[i] = CharToLower(gs_CurrentMap[i]);
	
	g_bEditMode = false;
	
	SQL_LoadSpawns(true); // Load Spawns
	
	//Precahche models for edit mode
	g_precacheLaser = PrecacheModel("materials/sprites/laserbeam.vmt");
	g_precacheGlow = PrecacheModel("materials/sprites/blueflare1.vmt");
	
	g_fMapLoadTime = GetEngineTime();

	g_iRoundType = RT_PISTOL;
	g_iWinRowCounter = 0;
	
	g_bScramble = false;
	g_bReplaceBomber = false;
	g_bSwapTeams = false;
	
	ResetPointsAll();
	
	LoopClients(i)
		DisableObserverMode(i);
}

public Action Event_OnFullConnect(Handle event, const char[] name, bool dontBroadcast) 
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
		
	// Don't force teams, this sucks valve
	if(GetClientCountEx(true) >= 2)
		SetEntPropFloat(client, Prop_Send, "m_fForceTeam", 3600.0);
		
	// If the cookies failed to load yet
	if(!g_bPreferenceLoaded[client])
		Command_Guns(client, 0);
	
	return Plugin_Continue;
}

public void OnClientDisconnect_Post(int client)
{
	g_bWarnned[client] = false;
	g_bPreferenceLoaded[client] = false;
	
	DisableObserverMode(client);
	CleanUpQueue();
}

public Action Event_OnPlayerDamged(Handle event, const char[] name, bool dontBroadcast)
{
	int attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
	int dhealth = GetEventInt(event, "dmg_health");
	
	int victim = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(!victim || !attacker)
		return Plugin_Continue;
	
	if(attacker == victim)
		return Plugin_Continue;
	
	if(!IsClientInGame(victim) || !IsClientInGame(attacker))
		return Plugin_Continue;
	
	char sWeapon[32];
	GetEventString(event, "weapon", sWeapon, sizeof(sWeapon));
	
	int iHitgroup = GetEventInt(event, "hitgroup");
	char sHigroup[32];
	
	int iPoints = dhealth;
	
	if(iPoints > 200)
		iPoints = 200;
	
	if(iHitgroup == 1)
		sHigroup = "headshot";
	else if(iHitgroup == 2)
		sHigroup = "upper torso";
	else if(iHitgroup == 3)
		sHigroup = "lower torso";
	else if(iHitgroup == 4)
		sHigroup = "left arm";
	else if(iHitgroup == 5)
		sHigroup = "right arm";
	else if(iHitgroup == 6)
		sHigroup = "left leg";
	else if(iHitgroup == 7)
		sHigroup = "right leg";
	else sHigroup = "body";
	
	if(dhealth > 200)
		AddPoints(attacker, iPoints, "+ %i (%i) points of damage inflicted to %N with %s (%s)", iPoints, dhealth, victim, sWeapon, sHigroup);
	else AddPoints(attacker, iPoints, "+ %i points of damage inflicted to %N with %s (%s)", iPoints, victim, sWeapon, sHigroup);
	
	return Plugin_Continue;
}

public Action Event_Death(Handle event, const char[] name, bool dontBroadcast)
{
	int victim = GetClientOfUserId(GetEventInt(event, "userid"));
	int attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
	int assister = GetClientOfUserId(GetEventInt(event, "assister"));
	
	if(!victim || !attacker)
		return Plugin_Continue;
	
	if(attacker == victim)
		return Plugin_Continue;
	
	if(!IsClientInGame(victim) || !IsClientInGame(attacker))
		return Plugin_Continue;
	
	char sWeapon[32];
	GetEventString(event, "weapon", sWeapon, sizeof(sWeapon));
	bool bHeadshot = GetEventBool(event, "headshot");
	int iFOV = GetEntProp(attacker, Prop_Data, "m_iFOV");
	bool bNoScoped = (iFOV == 0 && (strcmp(sWeapon, "awp", false) == 0 || strcmp(sWeapon, "ssg08", false) == 0));
	
	int iPenetraded = GetEventInt(event, "penetrated");
	
	int iPoints;
	int pKill = 50;
	int pHeadshot = 50;
	int pNoScope = 50;
	int pPenetration = 34;
	int pAssist = 42;
	
	if(bHeadshot && bNoScoped && iPenetraded > 0)
	{
		iPoints = pKill + pHeadshot + pNoScope + (pPenetration * iPenetraded);
		AddPoints(attacker, iPoints, "+ %i points for killing %N with %s (headshot & noscope & penerated x%i)", iPoints, victim, sWeapon, iPenetraded);
	}
	else if(bHeadshot && bNoScoped)
	{
		iPoints = pKill + pHeadshot + pNoScope;
		AddPoints(attacker, iPoints, "+ %i points for killing %N with %s (headshot & noscope)", iPoints, victim, sWeapon);
	}
	else if(bHeadshot && iPenetraded > 0)
	{
		iPoints = pKill + pHeadshot + (pPenetration * iPenetraded);
		AddPoints(attacker, iPoints, "+ %i points for killing %N with %s (headshot & penerated x%i)", iPoints, victim, sWeapon, iPenetraded);
	}
	else if(bNoScoped && iPenetraded > 0)
	{
		iPoints = pKill + pNoScope + (pPenetration * iPenetraded);
		AddPoints(attacker, iPoints, "+ %i points for killing %N with %s (noscope & penerated x%i)", iPoints, victim, sWeapon, iPenetraded);
	}
	else if(bHeadshot)
	{
		iPoints = pKill + pHeadshot;
		AddPoints(attacker, iPoints, "+ %i points for killing %N with %s (headshot)", iPoints, victim, sWeapon);
	}
	else if(bNoScoped)
	{
		iPoints = pKill + pNoScope;
		AddPoints(attacker, iPoints, "+ %i points for killing %N with %s (noscope)", iPoints, victim, sWeapon);
	}
	else if(iPenetraded > 0)
	{
		iPoints = pKill + (pPenetration * iPenetraded);
		AddPoints(attacker, iPoints, "+ %i points for killing %N with %s (penerated x%i)", iPoints, victim, sWeapon, iPenetraded);
	}
	
	else 
	{
		iPoints = pKill;
		AddPoints(attacker, iPoints, "+ %i points for killing %N with %s", iPoints, victim, sWeapon);
	}
	
	// Print point stats msgs of our victim since he died
	PrintPointStats(victim);
	
	
	// Assister
	if(assister && assister != attacker && assister != victim && IsClientInGame(assister))
	{
		AddPoints(assister, pAssist, "+ %i points for assisting %N killing %N", pAssist, attacker, victim);
	}
	
	return Plugin_Continue;
}

public Action Event_OnRoundPreStart(Handle event, const char[] name, bool dontBroadcast)
{
	if(g_bEditMode)
		return Plugin_Continue;
	
	g_iBomb = 0;
	
	UpdateTeams();
	
	return Plugin_Continue;
}

public Action Event_OnRoundPostStart(Handle event, const char[] name, bool dontBroadcast)
{
	if(g_bEditMode || g_bWaitForPlayers)
		return Plugin_Continue;
	
	ResetSpawnsUsed();
	
	// Save round start time
	g_fStartRoundTime = GetGameTime();
	
	// Which site to play?
	g_iSite = GetRandomInt(0, 1);
	
	// AWP
	g_iAwpCt = 0;
	g_iAwpT = 0;
	
	int iAwpChance = GetClientCountEx(false)*g_cvAwpPerPlayerChance.IntValue;
	
	if (100 <= iAwpChance || iAwpChance <= GetRandomInt(1, 100))
	{
		g_iAwpCt = GetRandomAwpPlayer(CS_TEAM_CT);
		g_iAwpT = GetRandomAwpPlayer(CS_TEAM_T);
	}
	
	// Round type
	
	// First round or pistol round
	int iRounds = CS_GetTeamScore(CS_TEAM_CT) + CS_GetTeamScore(CS_TEAM_T);
	if(g_cvPistolRoundsCount.IntValue > 0 && iRounds < g_cvPistolRoundsCount.IntValue)
	{
		g_iRoundType = RT_PISTOL;
		CPrintToChatAll("%s{darkred}Pistol round", PREFIX);
		
		if(iRounds == 0)
			PlayRoundStartSound(RT_FIRST);
		else PlayRoundStartSound(RT_PISTOL);
	}
	// FUN round
	else if(g_cvFunRoundsCount.IntValue > 0 && iRounds < g_cvPistolRoundsCount.IntValue + g_cvFunRoundsCount.IntValue)
	{
		g_iRoundType = RT_FUN;
		CPrintToChatAll("%s%s", PREFIX, g_sDoom[GetRandomInt(0, sizeof(g_sDoom)-1)]);
		
		PlayRoundStartSound(RT_FUN);
	}
	// Force buy round
	else if(g_cvWinRowForce.IntValue > 0 && g_cvWinRowForce.IntValue <= g_iWinRowCounter)
	{
		g_iRoundType = RT_FORCE;
		CPrintToChatAll("%s{darkred}Force Buy", PREFIX);
		
		PlayRoundStartSound(RT_FORCE);
	}
	// Full buy round
	else 
	{
		g_iRoundType = RT_FULL;
		CPrintToChatAll("%s{darkred}Full Buy.", PREFIX);
			
		PlayRoundStartSound(RT_FULL);
	}
	
	// Grenade selection
	SelectGrenades();
	
	// Bomber
	g_iBomber = GetRandomPlayerFromTeam(CS_TEAM_T);
	g_bBombPlanted = false;
	
	// Teleport and equip all players
	
	LoopIngameClients(iClient)
	{
		if(IsClientSourceTV(iClient) || GetClientTeam(iClient) <= CS_TEAM_SPECTATOR)
			continue;
		
		ResetEquipment(iClient);
		TeleportPlayer(iClient);
		
		if(iClient == g_iBomber)
		{
			g_iBomb = GivePlayerItem(iClient, "weapon_c4");
			EquipPlayerWeapon(iClient, g_iBomb);
		}
		else GiveEquipment(iClient);
		
		// Paint screen with team color
		if(g_cvFade.BoolValue)
			Fade(iClient, 1500, 0, 0x0001 | 0x0008, g_iRoundType == RT_FUN);
	}
	
	ResetPointsAll();
	
	// Announce bomb site in chat
	CPrintToChatAll("%sRetake on Site: {orange}>{grey}>{orange}> {darkred}%s {orange}<{grey}<{orange}<", PREFIX, g_iSite == SITEA ? "A":"B");
	
	return Plugin_Continue;
}

char sRoundTypes[4][255] =  {"Pistol", "Fun", "Force", "Normal" };

public Action Timer_PrintHintSite(Handle timer, any data)
{
	// Bottom Center
	if(GetGameTime() - g_fStartRoundTime < 8)
	{
		LoopIngamePlayers(iClient)
		{
			if(g_iHUDmode[iClient] != 2 && g_iHUDmode[iClient] != 3)
				continue;
			
			PrintHintText(iClient, "       <font size='22'> <font color = '#004db8'>%d CT</font> vs <font color = '#FE2E2E'> %d T</font>\n Retake on Site: <font color='#ddd40e'>%s</font></font>", GetTeamClientCountFix(CS_TEAM_CT), GetTeamClientCountFix(CS_TEAM_T), g_iSite == SITEA ? "A":"B");
		}
	}
	
	// Under radar
	char sText[512];
	if(g_bWaitForPlayers)
	{
		Format(sText, sizeof(sText), "Waiting for players ...");
		
		LoopIngamePlayers(iClient)
		{
			if(g_iHUDmode[iClient] != 1 && g_iHUDmode[iClient] != 3)
				continue;
			
			ShowGameText(iClient, 2, { 255, 255, 0, 255 }, 0.02, 0.35, 1.1, sText);
		}
	}
	else
	{
		Format(sText, sizeof(sText), "Site: %s\nRoundType: %s", g_iSite == SITEA ? "A":"B", sRoundTypes[g_iRoundType]);
		
		if(GetGameTime() - g_fStartRoundTime < 8)
		{
			LoopIngamePlayers(iClient)
			{
				if(g_iHUDmode[iClient] != 1 && g_iHUDmode[iClient] != 3)
					continue;
				
				int iTeam = GetClientTeam(iClient);
				ShowGameText(iClient, 2, iTeam <= 1 ? { 255, 20, 255, 255 } : (iTeam == CS_TEAM_CT ? { 20, 20, 255, 255 } : { 255, 20, 20, 255 }), 0.02, 0.35, 1.1, sText);
			}
		}
	}
	
	// Bomb beacon
	if(GetGameTime() - g_fStartRoundTime < 6)
	{
		if(!g_bBombPlanted && g_iBomber > 0 && IsClientInGame(g_iBomber) && IsPlayerAlive(g_iBomber))
			GetClientAbsOrigin(g_iBomber, g_fBombPos);
		
		LoopAlivePlayers(iClient)
		{
			if(iClient == g_iBomber)
				continue;
			
			if(GetClientTeam(iClient) == CS_TEAM_CT)
				continue;
			
			TE_SetupBeamRingPoint(g_fBombPos, 10.0, 160.0, g_iBombRing, g_iHalo, 0, 10, 0.6, 10.0, 0.5, {255, 255, 255, 255}, 5, 0);
			TE_SendToClient(iClient);
		}
	}
	
	return Plugin_Continue;
}

stock void ShowGameText(int iClient, int iChannel, int color[4], float x, float y, float time, char[] sText)
{
	SetHudTextParamsEx(x, y, time, color, _, 0, 0.0, 0.0, 0.0);
	ShowHudText(iClient, iChannel, sText);
}

public Action Event_OnRoundEnd(Handle event, const char[] name, bool dontBroadcast)
{
	if(g_bEditMode)
		return Plugin_Continue;
	
	g_iWinnerTeam = GetEventInt(event, "winner");
	
	// Bomber failed to plant the bomb
	if(!g_bBombPlanted && g_iBomber > 0 && g_iWinnerTeam == CS_TEAM_CT)
	{
		ReplaceBomberOnNextUpdate();
	}
	else
	{
		// CTs win
		if(g_iWinnerTeam == CS_TEAM_CT)
		{
			//CPrintToChatAll("%s{darkblue}>>> CT wins <<<", PREFIX);
			g_iWinRowCounter = 0;
			SwapTeamsOnNextUpdate();
		}
		// Ts win
		else if (g_iWinnerTeam == CS_TEAM_T)
		{
			if(g_iRoundType != RT_PISTOL && g_iRoundType != RT_FUN)
			{
				g_iWinRowCounter++;
				
				//CPrintToChatAll("%s{darkred}>>> T wins <<<", PREFIX);
			}
			else g_iWinRowCounter = 0;
			
			if(g_cvWinRowScramble.IntValue > 0)
			{
				// Scramble Players
				if(g_iWinRowCounter == g_cvWinRowScramble.IntValue || g_bScrambling)
				{
					g_iWinRowCounter = 0;
					ScrambleTeamsOnNextUpdate();
				}
				// Announce incoming scramble
				else if(g_iWinRowCounter > g_cvWinRowScramble.IntValue - 3)
					CPrintToChatAll("%sThe terrorists need to win {darkred}%d{lime} more rounds in a row to {purple}scramble {darkblue}teams{lime}", PREFIX, g_cvWinRowScramble.IntValue - g_iWinRowCounter);
			}
			
			LoopIngameClients(i)
			{
				if(GetClientTeam(i) != CS_TEAM_CT)
					continue;
				
				if(!IsPlayerAlive(i))
					continue;
				
				AddPoints(i, -200, "- 200 points for being alive while the bomb has been exploded");
			}
		}
	}
	
	PrintPointStatsTop();
	
	// Print point stats msgs
	LoopIngamePlayers(iClient)
		PrintPointStats(iClient);
	
	return Plugin_Continue;
}

public Action Event_OnItemEquip(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	g_iWepType[client] = GetEventInt(event, "weptype");
	
	return Plugin_Continue;
}