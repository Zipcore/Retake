char sndFun[][] = {
	"retake/GLaDOS/bloodbaths.mp3",
	"retake/GLaDOS/bored.mp3",
	"retake/GLaDOS/farce.mp3",
	"retake/GLaDOS/new.mp3",
	"retake/GLaDOS/random.mp3"
};

char sndEnemyForceBuy[] = "retake/3kliks/enemy_force_buy_round.mp3";
char sndFirst[] = "retake/3kliks/first_round.mp3";
char sndForceBuy[] = "retake/3kliks/force_buy_round.mp3";
char sndFullBuy[] = "retake/3kliks/full_buy_round.mp3";
char sndPistol[] = "retake/3kliks/pistol_round.mp3";
//char sndTheEnd[] = "retake/3kliks/the_end.mp3";

void PlayRoundStartSound(int roundtype)
{
	if(g_hTimerSound != null)
		CloseHandle(g_hTimerSound);
	
	if(g_cvSoundsEnable.BoolValue)
		g_hTimerSound = CreateTimer(2.1, Timer_PlayRoundStartSound, roundtype, TIMER_FLAG_NO_MAPCHANGE);
}

public Action Timer_PlayRoundStartSound(Handle timer, any data)
{
	g_hTimerSound = null;
	
	LoopIngameClients(client)
	{
		if (g_fVolume[client] <= 0.0)
			continue;
		
		switch(data)
		{
			case RT_FIRST: EmitSoundToClient(client, sndFirst, _, _, _, _, g_fVolume[client]);
			case RT_PISTOL: EmitSoundToClient(client, sndPistol, _, _, _, _, g_fVolume[client]);
			case RT_FUN: 
			{
				EmitSoundToClient(client, sndFun[GetRandomInt(0, 4)], _, _, _, _, g_fVolume[client]);
			}
			case RT_FULL: EmitSoundToClient(client, sndFullBuy, _, _, _, _, g_fVolume[client]);
			case RT_FORCE: 
			{
				if(GetClientTeam(client) == CS_TEAM_CT)
					EmitSoundToClient(client, sndEnemyForceBuy, _, _, _, _, g_fVolume[client]);
				else EmitSoundToClient(client, sndForceBuy, _, _, _, _, g_fVolume[client]);
			}
		}
	}
	
	return Plugin_Handled;
}

/* void PlayTheEnd()
{
	LoopIngameClients(client)
		if(g_bSounds[client])
			EmitSoundToClientAny(client, sndTheEnd);
} */

void PrepareSounds()
{
	if(!g_cvSoundsEnable.BoolValue)
		return;
	
	PrecacheSoundAnyDownload(sndFun[0]);
	PrecacheSoundAnyDownload(sndFun[1]);
	PrecacheSoundAnyDownload(sndFun[2]);
	PrecacheSoundAnyDownload(sndFun[3]);
	PrecacheSoundAnyDownload(sndFun[4]);
	
	PrecacheSoundAnyDownload(sndEnemyForceBuy);
	PrecacheSoundAnyDownload(sndFirst);
	PrecacheSoundAnyDownload(sndForceBuy);
	PrecacheSoundAnyDownload(sndFullBuy);
	PrecacheSoundAnyDownload(sndPistol);
	//PrecacheSoundAnyDownload(sndTheEnd);
}

void PrecacheSoundAnyDownload(char[] sSound)
{
	PrecacheSound(sSound);
	
	char sBuffer[256];
	Format(sBuffer, sizeof(sBuffer), "sound/%s", sSound);
	AddFileToDownloadsTable(sBuffer);
}