public void Event_PlayerBlind(Handle event, const char[] name, bool db)
{
	if(g_iFlasher <= 0)
		return;
	
	int victim = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(victim <= 0)
		return;
	
	bool alive = IsPlayerAlive(victim);
	
	float alpha = GetEntPropFloat(victim, Prop_Send, "m_flFlashMaxAlpha");
	float duration = GetEntPropFloat(victim, Prop_Send, "m_flFlashDuration");
	 
	if(alpha >= ALPHA_LIMIT && duration >= DURATION_LIMIT)
	{
		if(!alive || g_iFlasher == victim)
			return;
		
		if(GetClientTeam(g_iFlasher) != GetClientTeam(victim))
			return;
		
		g_iTeamflashes++;
	}
}

public void Event_FlashbangDetonate(Handle event, const char[] name, bool db)
{							 	
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(g_iFlasher != 0)
	{
		g_fFlashDelay += 0.1;
		CreateTimer(g_fFlashDelay+0.1, ProcessFlashResultsDelayed);
	}
	else
	{
		ProcessFlashResults();
		g_iFlasher = 0;
	}
	
	g_iFlasher = client; 
	g_iTeamflashes = 0;
}

public Action ProcessFlashResultsDelayed(Handle timer)
{
	g_fFlashDelay -= 0.1;
	
	if(g_fFlashDelay <= 0.0)
		g_fFlashDelay = 0.0;
	
	ProcessFlashResults();
	return Plugin_Handled;
}

void ProcessFlashResults()
{
	if(GetTeamClientCountEx(CS_TEAM_CT, true) < 1 || GetTeamClientCountEx(CS_TEAM_CT, true) < 1)
		LoopAlivePlayers(iClient)
			SetEntPropFloat(iClient, Prop_Send, "m_flFlashMaxAlpha", 0.5);
	
	if(g_iTeamflashes > 0)
		AddPoints(g_iFlasher, g_iTeamflashes*(-15), "- %i points for (full) flashing your own teammates", 15*g_iTeamflashes);
	
	g_iFlasher = 0;
}
