Handle g_hSql = null;
int g_iReconncetCounter = 0;

void ConnectSQL()
{
	if (g_hSql != null)
		CloseHandle(g_hSql);
	
	g_hSql = null;
	
	if (SQL_CheckConfig("retakes"))
		SQL_TConnect(ConnectSQLCallback, "retakes");
	else SetFailState("PLUGIN STOPPED - Reason: No config entry found for 'retakes' in databases.cfg - PLUGIN STOPPED");
}

public void ConnectSQLCallback(Handle owner, Handle hndl, const char[] error, any data)
{
	if (g_iReconncetCounter >= 5)
	{
		LogError("PLUGIN STOPPED - Reason: reconnect counter reached max - PLUGIN STOPPED");
		return;
	}
	
	if (hndl == null)
	{
		LogError("Connection to SQL database has failed, Reason: %s", error);
		
		g_iReconncetCounter++;
		ConnectSQL();
		
		return;
	}
	
	char sDriver[16];
	SQL_GetDriverIdent(owner, sDriver, sizeof(sDriver));
	
	g_hSql = CloneHandle(hndl);		
	
	if (StrEqual(sDriver, "mysql", false))
		SQL_TQuery(g_hSql, CreateSQLTableCallback, "CREATE TABLE IF NOT EXISTS `spawns` (`id` int(11) NOT NULL AUTO_INCREMENT, `type` int(11) NOT NULL, `site` int(11) NOT NULL, `map` varchar(32) NOT NULL, `posx` float NOT NULL, `posy` float NOT NULL, `posz` float NOT NULL, `angx` float NOT NULL, PRIMARY KEY (`id`));");
	else SQL_TQuery(g_hSql, CreateSQLTableCallback, "CREATE TABLE IF NOT EXISTS `spawns` (`id` INTEGER PRIMARY KEY, `type` INTEGER NOT NULL, `site` INTEGER NOT NULL, `map` varchar(32) NOT NULL, `posx` float NOT NULL, `posy` float NOT NULL, `posz` float NOT NULL, `angx` float NOT NULL);");
	
	g_iReconncetCounter = 1;
}

public void CreateSQLTableCallback(Handle owner, Handle hndl, const char[] error, any data)
{
	if (owner == null)
	{		
		LogError("SQL:Reconnect");
		g_iReconncetCounter++;
		ConnectSQL();
		
		return;
	}
	
	if (hndl == null)
	{
		LogError("SQL CreateTable:%s", error);
		return;
	}
	
	SQL_LoadSpawns();
}

void SQL_LoadSpawns(bool mapstart = false)
{
	if (g_hSql == null)
		ConnectSQL();
	else 
	{
		char sQuery[384];
		FormatEx(sQuery, sizeof(sQuery), "SELECT id, type, site, posx, posy, posz, angx FROM spawns WHERE map = '%s'", gs_CurrentMap);
		SQL_TQuery(g_hSql, LoadSpawnsCallBack, sQuery, mapstart, DBPrio_High);
	}
}

public LoadSpawnsCallBack(Handle owner, Handle hndl, const char[] error, any data)
{
	if (hndl == null)
	{
		LogError("SQL Error on LoadDest: %s", error);
		return;
	}
	
	g_SpawnsCount = 0;
	while (SQL_FetchRow(hndl) && g_SpawnsCount < MAXSPAWNS)
	{		
		g_Spawns[g_SpawnsCount][Id] = SQL_FetchInt(hndl, 0);
		g_Spawns[g_SpawnsCount][Type] = SpawnType:SQL_FetchInt(hndl, 1);
		g_Spawns[g_SpawnsCount][Site] = SQL_FetchInt(hndl, 2);
		g_Spawns[g_SpawnsCount][Location][0] = SQL_FetchFloat(hndl, 3);
		g_Spawns[g_SpawnsCount][Location][1] = SQL_FetchFloat(hndl, 4);
		g_Spawns[g_SpawnsCount][Location][2] = SQL_FetchFloat(hndl, 5);
		g_Spawns[g_SpawnsCount][Angles][0] = 0.0;
		g_Spawns[g_SpawnsCount][Angles][1] = SQL_FetchFloat(hndl, 6);
		g_Spawns[g_SpawnsCount][Angles][2] = 0.0;

		g_SpawnsCount++;
	}

	if(data)
	{
		if(g_SpawnsCount == 0)
		{
			g_bEditMode = true;
			CPrintToChatAll("%sEdit mode is now Enabled becuase there is no spawns", PREFIX);
			CreateTimer(0.1, DrawSpawns, _, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
		}
		else g_bEditMode = false;
	}
}

void SQL_AddSpawn(int client, int type)
{
	char sQuery[512];
	float loc[3];
	float ang[3];
	GetClientAbsOrigin(client, loc);
	GetClientEyeAngles(client, ang);
	FormatEx(sQuery, sizeof(sQuery), "INSERT INTO spawns (map, type, site, posx, posy, posz, angx) VALUES ('%s', '%d', '%d', %f, %f, %f, %f);", gs_CurrentMap, type, g_iChoosedSite[client], loc[0], loc[1], loc[2], ang[1]);
	SQL_TQuery(g_hSql, AddSpawnCallback, sQuery, client, DBPrio_Normal);
}

void SQL_AddSpawnEx(int client, int type, float loc[3], float ang[3], int site)
{
	char sQuery[512];
	FormatEx(sQuery, sizeof(sQuery), "INSERT INTO spawns (map, type, site, posx, posy, posz, angx) VALUES ('%s', '%d', '%d', %f, %f, %f, %f);", gs_CurrentMap, type, site, loc[0], loc[1], loc[2], ang[1]);
	SQL_TQuery(g_hSql, AddSpawnCallback2, sQuery, client, DBPrio_Normal);
}

public void AddSpawnCallback(Handle owner, Handle hndl, const char[] error, any data)
{
	if (hndl == null)
	{
		LogError("SQL Error on AddSpawn: %s", error);
		return;
	}

	Command_AddSpawn(data, 0);
	SQL_LoadSpawns();
}

public void AddSpawnCallback2(Handle owner, Handle hndl, const char[] error, any data)
{
	if (hndl == null)
	{
		LogError("SQL Error on AddSpawn: %s", error);
		return;
	}
}

void SQL_DeleteSpawn(int client, int spawnNum)
{
	char sQuery[64];
	FormatEx(sQuery, sizeof(sQuery), "DELETE FROM spawns WHERE id = %d", g_Spawns[spawnNum][Id]);
	SQL_TQuery(g_hSql, DeleteSpawnCallBack, sQuery, client, DBPrio_High);
}

public int DeleteSpawnCallBack(Handle owner, Handle hndl, const char[] error, any data)
{
	if (hndl == null)
	{
		LogError("SQL Error on DeleteSpawn: %s", error);
		return;
	}
	
	SQL_LoadSpawns();
	
	if (IsClientInGame(data))
		CPrintToChat(data, "%sDeleted spawn", PREFIX);
}