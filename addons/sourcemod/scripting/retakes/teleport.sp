int g_Spawns[MAXSPAWNS][Spawn];
int g_SpawnsCount = 0;

void TeleportPlayer(int client)
{
	int team = GetClientTeam(client);
	
	int randomSpawn = -1;
	float loc[3];
	float ang[3];
	
	if(client != g_iBomber)
	{
		randomSpawn = GetRandomSpawn(team == CS_TEAM_CT ? Ct : T, g_iSite);
		if(randomSpawn != -1)
		{
			Array_Copy(g_Spawns[randomSpawn][Location], loc, 3);
			Array_Copy(g_Spawns[randomSpawn][Angles], ang, 3);
			g_Spawns[randomSpawn][Used] = true;
			TeleportEntity(client, loc, ang, NULL_VECTOR);
		}
		else LogError("Not enough normal spawn points available for team %s and site: %s", team == CS_TEAM_CT ? "CT" : "T", g_iSite == 0 ? "A" : "B");
	}
	else
	{
		randomSpawn = GetRandomSpawn(Bomb, g_iSite);
		if(randomSpawn != -1)
		{
			Array_Copy(g_Spawns[randomSpawn][Location], loc, 3);
			Array_Copy(g_Spawns[randomSpawn][Angles], ang, 3);
			g_Spawns[randomSpawn][Used] = true;
			TeleportEntity(client, loc, ang, NULL_VECTOR);
		}
		else LogError("Not enough bomber spawn points available for site: %s", g_iSite == 0 ? "A" : "B");
	}
}

int GetRandomSpawn(SpawnType type, int site)
{
	int iSpawns[MAXPLAYERS+1];
	int numSpawns;

	for (int i = 0; i < g_SpawnsCount; i++)
	{
		if(!g_Spawns[i][Used] && g_Spawns[i][Type] == type && g_Spawns[i][Site] == site)
		{
			iSpawns[numSpawns] = i;
			numSpawns++;
		}
	}

	return numSpawns ? iSpawns[GetRandomInt(0, numSpawns-1)] : -1;
}

void ResetSpawnsUsed()
{
	for (int i = 0; i < g_SpawnsCount; i++)
	{
		g_Spawns[i][Used] = false;
	}
}