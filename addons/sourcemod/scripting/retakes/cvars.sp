ConVar g_cvDebug = null;
ConVar g_cvPistolRoundsCount = null;
ConVar g_cvFunRoundsCount = null;
ConVar g_cvWinRowForce = null;
ConVar g_cvWinRowScramble = null;
ConVar g_cvQueue = null;
ConVar g_cvBalance = null;
ConVar g_cvSwapSimple = null;
ConVar g_cvMoveWorst = null;
ConVar g_cvStrongPistolChance = null;
ConVar g_cvDeaglePistolChance = null;
ConVar g_cvTaserChance = null;
ConVar g_cvDidntPlant = null;
ConVar g_cvDidntPlantWarn = null;
ConVar g_cvAwpPerPlayerChance = null;
ConVar g_cvAwpPrioVIP = null;
ConVar g_cvFade = null;
ConVar g_cvForcePlant = null;
ConVar g_cvInstantPlant = null;
ConVar g_cvMaxPlayersT = null;
ConVar g_cvMaxPlayersCT = null;
ConVar g_cvSoundsEnable = null;
ConVar g_cvPriorityQueue = null;
ConVar g_cvKevlarVIP = null;
ConVar g_cvDefuseKit = null;
ConVar g_cvDefuseKitChance = null;
ConVar g_cvAugVipOnly = null;

void CreateCvars()
{
	//Convars
	CreateConVar("retakes_version", PL_VERSION, "Retakes plugin version", FCVAR_SPONLY);
	
	g_cvDebug = CreateConVar("retakes_debug", "0", "Enable debug mode", _, true, 0.0, true, 1.0);
	
	g_cvPistolRoundsCount = CreateConVar("retakes_pistols", "4", "How much Pistols Rounds should be. 0 to disable", _, true, 0.0);
	g_cvFunRoundsCount = CreateConVar("retakes_fun", "1", "How much Fun Rounds should be. 0 to disable", _, true, 0.0);
	g_cvWinRowForce = CreateConVar("retakes_winrow_force", "2", "How much row the T's need to win for force buy Ts (Counts only full & force rounds). 0 to disable", _, true, 0.0);
	g_cvWinRowScramble = CreateConVar("retakes_winrow_scramble", "4", "How much row the T's need to win for scramble (Counts only full & force rounds). 0 to disable", _, true, 0.0);
	
	g_cvQueue = CreateConVar("retakes_queue", "1", "Use queue system", _, true, 0.0, true, 1.0);
	g_cvBalance = CreateConVar("retakes_balance", "1", "Balance teams on round start pre", _, true, 0.0, true, 1.0);
	g_cvSwapSimple = CreateConVar("retakes_simple_swap", "0", "If enabled swaps the whole team (no team limit checks)", _, true, 0.0, true, 1.0);
	g_cvMoveWorst = CreateConVar("retakes_enqueue_worst", "1", "Move worst player of the loosing team to spec, when tehre are player in queue", _, true, 0.0, true, 1.0);
	
	g_cvDidntPlant = CreateConVar("retakes_didntplant", "-1", "How much time player should be banned if he didnt planted twice. -1: Do nothing, 0: Move to CT, 1: Kick client, 2: Ban client", _, true, -1.0);
	g_cvDidntPlantWarn = CreateConVar("retakes_didntplant_warn", "0", "Warn the player once before perform punishment", _, true, 0.0, true, 1.0);
	
	g_cvFade = CreateConVar("retakes_fade", "1", "Fade team color on screen when spawning", _, true, 0.0, true, 1.0);
	
	g_cvForcePlant = CreateConVar("retakes_force_plant", "1", "Force the player to plant the bomb", _, true, 0.0, true, 1.0);
	g_cvInstantPlant = CreateConVar("retakes_instant_plant", "1", "Speed up planting the bomb", _, true, 0.0, true, 1.0);
	
	g_cvMaxPlayersT = CreateConVar("retakes_max_t_players", "4", "Max players on terrorist team", _, true, 3.0, true, 16.0);
	g_cvMaxPlayersCT = CreateConVar("retakes_max_ct_players", "5", "Max players on terrorist team", _, true, 3.0, true, 16.0);
	g_cvPriorityQueue = CreateConVar("retakes_queue_priority", "0", "Use priority sysytem for queue. Priority for players with 'a' flag.", _, true, 0.0, true, 1.0);
	
	g_cvSoundsEnable = CreateConVar("retakes_sounds_enable", "1", "Enables round start sounds (can be still disabled by guns menu)", _, true, 0.0, true, 1.0);
	
	g_cvStrongPistolChance = CreateConVar("retakes_strongchance", "33", "How much percent to give a awper a strong pistol by his choice. 0 to disable", _, true, 0.0, true, 100.0);
	g_cvDeaglePistolChance = CreateConVar("retakes_deaglechance", "5", "Chance to get a a deagle instead of a strong pistol is selected. 0 to disable", _, true, 0.0, true, 100.0);
	
	g_cvTaserChance = CreateConVar("retakes_taserchance", "0", "Chance to get a taser. 0 to disable", _, true, 0.0, true, 100.0);
	
	g_cvAwpPerPlayerChance = CreateConVar("retakes_awp_perplayer_chance", "15", "Chance to have an AWP on both teams per player", _, true, 0.0, true, 100.0);
	
	g_cvKevlarVIP = CreateConVar("retakes_kevlar_vip", "2", "VIP only - 0: Disable; 1: Give kevlar; 2: Give kevlar+helmet", _, true, 0.0, true, 2.0);
	g_cvAwpPrioVIP = CreateConVar("retakes_awp_mode", "2", "0: Random; 1: Prio. most points; 2: Prio. VIP players and points", _, true, 0.0, true, 2.0);
	
	g_cvDefuseKit = CreateConVar("retakes_defusekit", "2", "0: Always; 1: Random on force buy; 2: Same as 1, but always for VIPs", _, true, 0.0, true, 2.0);
	g_cvDefuseKitChance = CreateConVar("retakes_defusekit_chance", "50", "Chance to give defusekit to CTs on force buy rounds", _, true, 0.0, true, 100.0);
	
	g_cvAugVipOnly = CreateConVar("retakes_aug_vip", "1", "Make SG553 & Aig VIP only", _, true, 0.0, true, 1.0);
	
	AutoExecConfig(true, "retakes");
}