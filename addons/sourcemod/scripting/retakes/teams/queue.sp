Handle g_aQueue = null;

bool g_bObserve[MAXPLAYERS + 1];


void QueueClients()
{
	if(!g_cvQueue.BoolValue)
		return;
	
	LoopIngameClients(iClient)
	{
		if(IsClientInQueue(iClient))
			continue;
		
		if(GetClientTeam(iClient) > CS_TEAM_SPECTATOR)
			continue;
		
		CPrintToChat(iClient, "%s{green}You have to select a team for being able to play.", PREFIX);
	}
	
	int iCountCT, iCountT;
	
	int next;
	while((next = GetClientQueueNext()) != -1)
	{
		int team = GetWeekerTeam(false);
		
		// Teams are full
		if(team == CS_TEAM_NONE)
			break;
		
		RemoveFromArray(g_aQueue, 0); // Remove him from queue
		MovePlayerToTeam(next, team);
		
		if(team == CS_TEAM_CT)
			iCountCT++;
		else iCountT++;
	}
	
	if(g_cvDebug.BoolValue)
		LogMessage("[RETAKE-QUEUE] Moved %d players to CT and %d to T.", iCountCT, iCountT);
	
	LoopArray(iIndex, g_aQueue)
	{
		int iClient = GetClientOfUserId(GetArrayCell(g_aQueue, iIndex));
		
		if(iClient)
			CPrintToChat(iClient, "%sQueue position: {darkred}%i", PREFIX, iIndex+1);
	}
}

int AddClientToQueue(int iClient)
{
	CleanUpQueue();
	
	if(g_bObserve[iClient])
		return -1;
	
	if(IsClientSourceTV(iClient))
		return -1;
	
	if(IsClientInQueue(iClient))
		return GetClientQueuePosition(iClient);
	
	if(GetClientTeam(iClient) > CS_TEAM_SPECTATOR)
		return -1;
		
	PushArrayCell(g_aQueue, GetClientUserId(iClient));
	
	CleanUpQueue();
	
	int iPos = GetClientQueuePosition(iClient);
	
	CPrintToChat(iClient, "%sQueue position: {darkred}%i", PREFIX, iPos);
	
	return iPos;
}

void ToggleObserverMode(int iClient)
{
	g_bObserve[iClient] = !g_bObserve[iClient];
	
	CPrintToChat(iClient, "%s{darkblue}Observer mode %s{darkblue}.", PREFIX, g_bObserve[iClient] ? "{lime}activated" : "{darkred}deactivated");
	
	if(g_bObserve[iClient])
		MovePlayerToTeam(iClient, CS_TEAM_SPECTATOR);
	else AddClientToQueue(iClient);
}

void EnableObserverMode(int iClient)
{
	g_bObserve[iClient] = true;
	CPrintToChat(iClient, "%s{darkblue}Observer mode {lime}activated{darkblue}.", PREFIX);
}

void DisableObserverMode(int iClient)
{
	g_bObserve[iClient] = false;
	
	if(IsClientInGame(iClient))
		CPrintToChat(iClient, "%s{darkblue}Observer mode {darkred}deactivated{darkblue}.", PREFIX);
}

bool IsClientInQueue(int iClient)
{
	CleanUpQueue();
	
	LoopArray(i, g_aQueue)
	{
		int iClient2 = GetClientOfUserId(GetArrayCell(g_aQueue, i));
		
		// Player already in queue
		if(iClient == iClient2)
			return true;
	}
	
	return false;
}

int GetClientQueuePosition(int iClient)
{
	CleanUpQueue();
	
	LoopArray(i, g_aQueue)
	{
		int iClient2 = GetClientOfUserId(GetArrayCell(g_aQueue, i));
		
		// Player already in queue
		if(iClient == iClient2)
			return i+1;
	}
	
	return -1;
}

// Return the next client from queue and dequeues him
int GetClientQueueNext()
{
	CleanUpQueue();
	
	if(GetArraySize(g_aQueue) < 1)
		return -1;
	
	int iClient = GetClientOfUserId(GetArrayCell(g_aQueue, 0));
	
	if(iClient)
		return iClient;
	else LogError("GetClientQueueNext() reported: Client was not ingame.");
	
	return -1;
}

void CleanUpQueue()
{
	if(g_aQueue == null)
		g_aQueue = CreateArray(1);
	
	LoopArray(i, g_aQueue)
	{
		int iClient = GetClientOfUserId(GetArrayCell(g_aQueue, i));
		
		// Client is ingame and not observing
		if(iClient && !g_bObserve[iClient] && GetClientTeam(iClient) <= CS_TEAM_SPECTATOR && !IsClientSourceTV(iClient))
			continue;
		
		// Player is not ingame anymore, has joined a team or is observing
		RemoveFromArray(g_aQueue, i);
		i--;
	}
	
	if(g_cvPriorityQueue.BoolValue)
		SortADTArrayCustom(g_aQueue, SortQueueVIP);
}

public int SortQueueVIP(int index1, int index2, Handle array, Handle hndl)
{
    int iClient1 = GetClientOfUserId(GetArrayCell(array, index1));
    int iClient2 = GetClientOfUserId(GetArrayCell(array, index2));
    
    bool bClient1 = Client_HasAdminFlags(iClient1, ADMFLAG_RESERVATION);
    bool bClient2 = Client_HasAdminFlags(iClient2, ADMFLAG_RESERVATION);
    
    int sort;
    
    if(!bClient1 && bClient2)
    	sort = 1;
    else if(bClient1 && !bClient2)
    	sort = -1;
    
    return sort;
}