bool g_bScramble;
bool g_bReplaceBomber;
bool g_bSwapTeams;

void ScrambleTeamsOnNextUpdate()
{
	g_bScramble = true;
}

void ReplaceBomberOnNextUpdate()
{
	g_bReplaceBomber = true;
}

void SwapTeamsOnNextUpdate()
{
	g_bSwapTeams = true;
}

void ReplaceWorstNow()
{
	if(!g_cvMoveWorst.BoolValue)
		return;
	
	int iNext = GetClientQueueNext();
	
	if(iNext == -1)
		return;
	
	// We don't need todo this aslong teams have free slots
	if(HasTeamFreeSlot(CS_TEAM_CT) || HasTeamFreeSlot(CS_TEAM_T))
		return;
	
	Handle aPlayers = CreateArray(1);
	LoopIngameClients(i)
	{
		if(IsClientSourceTV(i))
			continue;
		
		int team = GetClientTeam(i);
		
		if(team <= CS_TEAM_SPECTATOR)
			continue;
			
		if(team == g_iWinnerTeam)
			continue;
		
		PushArrayCell(aPlayers, i);
	}
	
	SortADTArrayCustom(aPlayers, SortPlayersByPointsDescButVip);
	
	int iTarget = GetArrayCell(aPlayers, GetArraySize(aPlayers) - 1);
	
	MovePlayerToTeam(iNext, GetClientTeam(iTarget));
	
	MovePlayerToTeam(iTarget, CS_TEAM_SPECTATOR);
	DisableObserverMode(iTarget);
	AddClientToQueue(iTarget);
	
	CPrintToChat(iTarget, "%s{purple}You have been {orange}moved to {darkred}queue{purple}, {lime}we are sorry{purple} but others like to play aswell.", PREFIX);
	
	delete aPlayers;
}

void ScrambleTeamNow()
{
	if(!g_bScramble)
		return;
	
	CPrintToChatAll("%s{purple}Scrambling teams", PREFIX);
	
	g_bScramble = false;
	g_bReplaceBomber = false;
	g_bSwapTeams = false;
	
	Handle aPlayers = CreateArray(1);
	int iCountCT, iCountT;
	
	LoopIngameClients(i)
	{
		int team = GetClientTeam(i);
		if(team <= CS_TEAM_SPECTATOR || IsClientSourceTV(i))
			continue;
		
		PushArrayCell(aPlayers, i);
		
		if(team == CS_TEAM_CT)
			iCountCT++;
		else iCountT++;
	}
	
	// Randomize player list
	SortADTArrayCustom(aPlayers, SortPlayersByPointsDesc);
	
	iCountCT = 0;
	iCountT = 0;
	
	LoopArray(index, aPlayers)
	{
		int team;
		if(index == 0)
			team = CS_TEAM_CT;
		else team = GetWeekerTeam(true);
		
		int iClient = GetArrayCell(aPlayers, index);
		
		if(team == CS_TEAM_NONE)
		{
			MovePlayerToTeam(iClient, CS_TEAM_SPECTATOR);
			
			if(g_cvDebug.BoolValue)
				PrintToConsoleAll("### Retakes: Moved %N (%i points) to spectators, both teams are full (CTs: %d Ts: %d).", iClient, GetPoints(iClient), iCountCT, iCountT);
			
			continue;
		}
		
		MovePlayerToTeam(iClient, team);
		
		if(g_cvDebug.BoolValue)
			PrintToConsoleAll("### Retakes: Moved %N (%i points) to %s", iClient, GetPoints(iClient), team == CS_TEAM_CT ? "CT" : "T");
		
		if(team == CS_TEAM_CT)
			iCountCT++;
		else iCountT++;
	}
	
	delete aPlayers;
}

void ReplaceBomberNow()
{
	if(!g_bReplaceBomber)
		return;
	
	g_bReplaceBomber = false;
	
	if(g_bSwapTeams || g_iBomber < 1 || !IsClientInGame(g_iBomber))
		return;
		
	if(g_cvDebug.BoolValue)
		PrintToConsoleAll("### Retakes: Punish %N for not planting the bomb ###", g_iBomber);
	
	if(!g_bWarnned[g_iBomber] && g_cvDidntPlantWarn.BoolValue)
	{
		g_bWarnned[g_iBomber] = true;
		return;
	}
	
	switch(g_cvDidntPlant.IntValue)
	{
		case 0:
		{
			CPrintToChat(g_iBomber, "%s{purple}You {orange}failed to plant the {darkred}bomb{purple} and will be swapped to {darkblue}CT{purple}.", PREFIX, g_iBomber);
			
			MovePlayerToTeam(g_iBomber, CS_TEAM_CT);
		}
		case 1:
		{
			KickClient(g_iBomber, "You failed to plant the bomb");
		}
		case 2:
		{
			BanClient(g_iBomber, g_cvDidntPlant.IntValue, BANFLAG_AUTO, "Did not plant the bomb.", "You did not planted the bomb");
		}
	}
	
	g_bWarnned[g_iBomber] = false;
	
	if(g_cvDidntPlant.IntValue < 0)
		return;
	
	Handle aCTs = CreateArray(1);
	int iTs;
	LoopIngameClients(i)
	{
		int team = GetClientTeam(i);
		
		if(team <= CS_TEAM_SPECTATOR || IsClientSourceTV(i))
			continue;
			
		if(team != CS_TEAM_CT)
			PushArrayCell(aCTs, i);
		else iTs++;
	}
	
	int iCTs = GetArraySize(aCTs);
	
	// No need to switch a CT to T
	if(iCTs < 1 || iTs < iCTs)
		return;
	
	SortADTArrayCustom(aCTs, SortPlayersByPointsDesc);
	
	int iBestCT = GetArrayCell(aCTs, 0);
	MovePlayerToTeam(iBestCT, CS_TEAM_T);
	
	CPrintToChat(iBestCT, "%s{purple}You have been {orange}moved to {darkred}T{purple}.", PREFIX);
	
	delete aCTs;
}

void SwapTeamsNow()
{
	if(!g_bSwapTeams)
		return;
	
	if(g_cvDebug.BoolValue)
		PrintToConsoleAll("### Retakes: CTs WIN - Swap teams now ###");
	
	g_bSwapTeams = false;
	
	if(g_cvSwapSimple.BoolValue)
	{
		SwapTeamsSimpleNow();
		return;
	}
	
	Handle aCTs = CreateArray(1);
	Handle aTs = CreateArray(1);

	LoopIngameClients(i)
	{
		int team = GetClientTeam(i);
		if(team <= CS_TEAM_SPECTATOR || IsClientSourceTV(i))
			continue;
		
		if(team == CS_TEAM_CT)
			PushArrayCell(aCTs, i);
		else PushArrayCell(aTs, i);
	}
	
	SortADTArrayCustom(aCTs, SortPlayersByPointsDesc);
	SortADTArrayCustom(aTs, SortPlayersByPointsAsc);
	
	int iCount;
	
	// Move all CTs to T
	LoopArray(index, aCTs)
	{
		int iClient = GetArrayCell(aCTs, index);
		
		if(iCount >= GetArraySize(aTs))
		{
			if(g_cvDebug.BoolValue)
				PrintToConsoleAll("### Retakes: T team is full");
			break;
		}
		
		iCount++;
		
		MovePlayerToTeam(iClient, CS_TEAM_T);
		
		if(g_cvDebug.BoolValue)
			PrintToConsoleAll("### Retakes: Moved %N to T (%i points)", iClient, GetPoints(iClient));
	}
	
	// Move all Ts to CT
	LoopArray(index, aTs)
	{
		int iClient = GetArrayCell(aTs, index);
		MovePlayerToTeam(iClient, CS_TEAM_CT);
		
		if(g_cvDebug.BoolValue)
			PrintToConsoleAll("### Retakes: Moved %N to CT (%i points)", iClient, GetPoints(iClient));
	}
	
	CPrintToChatAll("%s{purple}Swapped teams", PREFIX);
	
	delete aCTs;
	delete aTs;
}

void SwapTeamsSimpleNow()
{
	LoopIngameClients(iClient)
	{
		int team = GetClientTeam(iClient);
		if(team <= CS_TEAM_SPECTATOR || IsClientSourceTV(iClient))
			continue;
		
		if(team == CS_TEAM_CT)
			MovePlayerToTeam(iClient, CS_TEAM_T);
		else MovePlayerToTeam(iClient, CS_TEAM_CT);
	}
}

void BalanceTeamsNow()
{
	if(!g_cvBalance.BoolValue)
		return;
	
	//PrintToConsoleAll("### Retakes: Balance teams ###");
	
	int iCTs;
	int iTs;

	LoopIngameClients(i)
	{
		int team = GetClientTeam(i);
		if(team <= CS_TEAM_SPECTATOR || IsClientSourceTV(i))
			continue;
		
		if(team == CS_TEAM_CT)
			iCTs++;
		else iTs++;
	}
	
	int iNewCTs = iCTs;
	int iNewTs = iTs;
	
	// More Ts as CTs
	if (iCTs < iTs)
	{
		while(iNewCTs < iNewTs && iNewTs > 1 && iNewCTs < g_cvMaxPlayersCT.IntValue)
		{
			iNewCTs++;
			iNewTs--;
		}
	}
	// More at least 2 more CTs as Ts
	else if (iTs+1 < iCTs)
	{
		while(iNewTs+1 < iNewCTs && iNewCTs > 1 && iNewTs < g_cvMaxPlayersT.IntValue)
		{
			iNewTs++;
			iNewCTs--;
		}
	}
	
	// No need to balanced
	if(iNewCTs == iCTs)
	{
		if(g_cvDebug.BoolValue)
			PrintToConsoleAll("### Retakes: Teams are already balanced (CTs: %d Ts: %d)", iCTs, iTs);
		
		return;
	}
	
	Handle aCTs = CreateArray(1);
	Handle aTs = CreateArray(1);

	LoopIngameClients(i)
	{
		int team = GetClientTeam(i);
		if(team <= CS_TEAM_SPECTATOR)
			continue;
		
		if(team == CS_TEAM_CT)
			PushArrayCell(aCTs, i);
		else PushArrayCell(aTs, i);
	}
	
	SortADTArrayCustom(aCTs, SortPlayersByPointsDesc);
	SortADTArrayCustom(aTs, SortPlayersByPointsAsc);
	
	// Move Ts to CT, starting with the lowest Ts
	if(iNewCTs > iCTs)
	{
		//PrintToConsoleAll("### Retakes: Start balancing teams. (CT: %d, T:%d) Moving %d Ts to CT", iCTs, iTs, iNewCTs - iCTs);
		
		for (int i = 0; i < iNewCTs - iCTs; i++)
		{
			int iClient = GetArrayCell(aTs, i);
			MovePlayerToTeam(iClient, CS_TEAM_CT);
			
			if(g_cvDebug.BoolValue)
				PrintToConsoleAll("### Retakes: Moved %N to CT (%i points)", iClient, GetPoints(iClient));
		}
	}
	// Move CTs to T, starting with the best CTs
	else
	{
		if(g_cvDebug.BoolValue)
			PrintToConsoleAll("### Retakes: Start balancing teams. (CT: %d, T:%d) Moving %d CTs to T", iCTs, iTs, iNewTs - iTs);
		
		for (int i = 0; i < iNewTs - iTs; i++)
		{
			int iClient = GetArrayCell(aCTs, i);
			MovePlayerToTeam(iClient, CS_TEAM_T);
			
			if(g_cvDebug.BoolValue)
				PrintToConsoleAll("### Retakes: Moved %N to T (%i points)", iClient, GetPoints(iClient));
		}
	}
	
	CPrintToChatAll("%s{purple}Balanced teams", PREFIX);
	
	delete aCTs;
	delete aTs;
}

public int SortPlayersByPointsDesc(int index1, int index2, Handle array, Handle hndl)
{
    int iClient1 = GetArrayCell(array, index1);
    int iClient2 = GetArrayCell(array, index2);
    
    int iPoints1 = GetPoints(iClient1);
    int iPoints2 = GetPoints(iClient2);
    
    int sort;
    
    if(iPoints1 < iPoints2)
    	sort = 1;
    else if (iPoints1 > iPoints2)
    	sort = -1;
    
    return sort;
}

public int SortPlayersByPointsDescButVip(int index1, int index2, Handle array, Handle hndl)
{
    int iClient1 = GetArrayCell(array, index1);
    int iClient2 = GetArrayCell(array, index2);
    
    int iPoints1 = GetPoints(iClient1);
    int iPoints2 = GetPoints(iClient2);
    
    int sort;
    
    if(iPoints1 < iPoints2)
    	sort = 1;
    else if (iPoints1 > iPoints2)
    	sort = -1;
    
    bool bClient1 = Client_HasAdminFlags(iClient1, ADMFLAG_RESERVATION);
    bool bClient2 = Client_HasAdminFlags(iClient2, ADMFLAG_RESERVATION);
    
    if(bClient1 != bClient2)
    {
        if(!bClient1 && bClient2)
            sort = 1;
        else sort = -1;
    }
    
    return sort;
}

public int SortPlayersByPointsAsc(int index1, int index2, Handle array, Handle hndl)
{
    int iClient1 = GetArrayCell(array, index1);
    int iClient2 = GetArrayCell(array, index2);
    
    int iPoints1 = GetPoints(iClient1);
    int iPoints2 = GetPoints(iClient2);
    
    int sort;
    
    if(iPoints1 > iPoints2)
    	sort = 1;
    else if (iPoints1 < iPoints2)
    	sort = -1;
    
    return sort;
}