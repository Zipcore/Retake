int g_iPoints[MAXPLAYERS + 1];
Handle g_aPointMsgs[MAXPLAYERS + 1];

void ResetPointsAll()
{
	LoopClients(iClient)
	{
		g_iPoints[iClient] = 0;
	}
}

int GetPoints(int iClient)
{
	return g_iPoints[iClient];
}

void AddPoints(int iClient, int points, char[] reason, any ...)
{
	if (g_aPointMsgs[iClient] == null)
		g_aPointMsgs[iClient] = CreateArray(512);
	
	char sFormat[512];
	VFormat(sFormat, sizeof(sFormat), reason, 4);
	
	g_iPoints[iClient] += points;
	
	PushArrayString(g_aPointMsgs[iClient], sFormat);
}

void PrintPointStatsTop()
{
	Handle aTs = CreateArray(1);
	Handle aCTs = CreateArray(1);
	
	int iTs, iCTs;
	
	LoopIngameClients(i)
	{
		int team = GetClientTeam(i);
		
		if(team == CS_TEAM_T)
		{
			PushArrayCell(aTs, i);
			iTs++;
		}
		else if(team == CS_TEAM_CT)
		{
			PushArrayCell(aCTs, i);
			iCTs++;
		}
	}
	
	SortADTArrayCustom(aTs, SortPlayersByPointsDesc);
	SortADTArrayCustom(aCTs, SortPlayersByPointsDesc);
	
	if(iTs > 3) 
		iTs = 3;
	
	if(iCTs > 3)
		iCTs = 3;
	
	char sBuffer[1024];
	
	LoopArray(i, aTs)
	{
		if(i > 3)
			break;
		
		int iClient = GetArrayCell(aTs, i);
		
		if(i == 0)
			Format(sBuffer, sizeof(sBuffer), "{darkred}Top %i Ts: ", iTs);
		
		if(i > 0)
			Format(sBuffer, sizeof(sBuffer), "%s{grey}, ", sBuffer);
		
		Format(sBuffer, sizeof(sBuffer), "%s{lime}%N {orange}(%i p)", sBuffer, iClient, GetPoints(iClient), sBuffer);
	}
	
	if(iTs > 0)
		CPrintToChatAll(sBuffer);
	
	LoopArray(i, aCTs)
	{
		if(i > 3)
			break;
		
		int iClient = GetArrayCell(aCTs, i);
		
		if(i == 0)
			Format(sBuffer, sizeof(sBuffer), "{darkblue}Top %i CTs: ", iCTs);
		
		if(i > 0)
			Format(sBuffer, sizeof(sBuffer), "%s{grey}, ", sBuffer);
		
		Format(sBuffer, sizeof(sBuffer), "%s{lime}%N {orange}(%i p)", sBuffer, iClient, GetPoints(iClient), sBuffer);
	}
	
	if(iCTs > 0)
		CPrintToChatAll(sBuffer);
	
	delete aTs;
	delete aCTs;
}

void PrintPointStats(int iClient)
{
	if(g_iPoints[iClient] <= 0)
		return;
	
	if(g_aPointMsgs[iClient] == null)
		return;
	
	if(GetArraySize(g_aPointMsgs[iClient]) <= 0)
		return;
	
	PrintToConsole(iClient, " ");
	PrintToConsole(iClient, "### Retakes: Round Stats ###");
	PrintToConsole(iClient, " ");
	
	LoopArray(iMsg, g_aPointMsgs[iClient])
	{
		char sMsg[512];
		GetArrayString(g_aPointMsgs[iClient], iMsg, sMsg, sizeof(sMsg));
		PrintToConsole(iClient, sMsg);
	}
	
	PrintToConsole(iClient, " ");
	
	PrintToConsole(iClient, "Total Points: %i", g_iPoints[iClient]);
	
	PrintToConsole(iClient, " ");
	
	ClearArray(g_aPointMsgs[iClient]);
}