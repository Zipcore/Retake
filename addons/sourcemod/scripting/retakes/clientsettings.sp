Handle g_hPrimaryWeapon = null;
Handle g_hWantAwp = null;
Handle g_hPistolPrefence = null;
Handle g_hVolume = null;
Handle g_hHUD = null;

char gs_WantPrimary[MAXPLAYERS + 1][64];
int g_iWantAwp[MAXPLAYERS+1] = {0, ...};
int g_iPistolPrefence[MAXPLAYERS+1] = {0, ...};
float g_fVolume[MAXPLAYERS + 1];
int g_iHUDmode[MAXPLAYERS+1] = {0, ...};

bool g_bPreferenceLoaded[MAXPLAYERS+1] = {false, ...};

void RegisterCookies()
{
	g_hWantAwp = RegClientCookie("retakes_v2_awp", "Retakes allow player play with awp", CookieAccess_Protected);
	g_hPrimaryWeapon = RegClientCookie("retakes_v2_primary", "Retakes play with which primary weapon", CookieAccess_Protected);
	g_hPistolPrefence = RegClientCookie("retakes_v2_pistol", "Retakes pistol prefernce", CookieAccess_Protected);
	g_hVolume = RegClientCookie("retakes_v2_volume", "Retakes sounds volume", CookieAccess_Protected);
	g_hHUD = RegClientCookie("retakes_v2_hud", "Retakes HUD mode", CookieAccess_Protected);
}

public void OnClientCookiesCached(int client)
{
	LoadClientCoockies(client);
}

void LoadClientCoockies(int client)
{
	if(IsFakeClient(client))
		return;
	
	char sBuffer[16];
	bool openGunsMenu = false;
	
	GetClientCookie(client, g_hWantAwp, sBuffer, sizeof(sBuffer));
	if(!StrEqual(sBuffer, ""))
		g_iWantAwp[client] = StringToInt(sBuffer);
	else openGunsMenu = true;
	
	GetClientCookie(client, g_hPrimaryWeapon, gs_WantPrimary[client], sizeof(gs_WantPrimary[]));
	if(strlen(gs_WantPrimary[client]) <= 1)
		openGunsMenu = true;
	
	GetClientCookie(client, g_hPistolPrefence, sBuffer, sizeof(sBuffer));
	if(!StrEqual(sBuffer, ""))
		g_iPistolPrefence[client] = StringToInt(sBuffer);
	else openGunsMenu = true;
	
	if(g_cvSoundsEnable.BoolValue)
	{
		GetClientCookie(client, g_hVolume, sBuffer, sizeof(sBuffer));
		if(!StrEqual(sBuffer, ""))
			g_fVolume[client] = StringToFloat(sBuffer);
		else 
		{
			g_fVolume[client] = 0.5;
			openGunsMenu = true;
		}
	}
	
	GetClientCookie(client, g_hHUD, sBuffer, sizeof(sBuffer));
	if(!StrEqual(sBuffer, ""))
		g_iHUDmode[client] = StringToInt(sBuffer);
	else openGunsMenu = true;

	g_bPreferenceLoaded[client] = !openGunsMenu;
}

void Pref_SetPrimary(int client, char value[64])
{
	strcopy(gs_WantPrimary[client], sizeof(gs_WantPrimary[]), value);
	SetClientCookie(client, g_hPrimaryWeapon, value);
}

void Pref_SetAWP(int client, char value[64])
{
	g_iWantAwp[client] = StringToInt(value);
	SetClientCookie(client, g_hWantAwp, value);
}

void Pref_SetPistol(int client, char value[64])
{
	g_iPistolPrefence[client] = StringToInt(value);
	SetClientCookie(client, g_hPistolPrefence, value);
}

void Pref_SetVolume(int client, char value[64])
{
	g_fVolume[client] = StringToFloat(value);
	SetClientCookie(client, g_hVolume, value);
}

void Pref_SetHUD(int client, char value[64])
{
	g_iHUDmode[client] = StringToInt(value);
	SetClientCookie(client, g_hHUD, value);
}