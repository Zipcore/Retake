#include "retakes/teams/queue.sp"
#include "retakes/teams/balance.sp"

bool g_bAllowTeamChange[MAXPLAYERS + 1];

void UpdateTeams()
{
	if(!g_bWaitForPlayers && !g_bEditMode)
	{
		// Replace worst player
		ReplaceWorstNow();
		
		// Scramble
		ScrambleTeamNow();
		
		// Replace bomber
		ReplaceBomberNow();
		
		// Swap teams
		SwapTeamsNow();
		
		// Balance
		BalanceTeamsNow();
	}
	
	// Queue
	QueueClients();
}

public Action Hook_ChangeTeamChange(int iClient, const char[] command, int iArgs)
{
	if(!g_cvQueue.BoolValue)
		return Plugin_Continue;
	
	if (iArgs < 1)
		return Plugin_Handled;
	
	// Allow bots to join anyway, cuz bots are retarded, thanks valve.
	if(IsFakeClient(iClient))
		return Plugin_Continue;
	
	// Edit mode, no normal gameplay possible anyway
	if(g_bEditMode)
		return Plugin_Continue;
	
	char arg[4];
	GetCmdArg(1, arg, sizeof(arg));
	
	int team_to = StringToInt(arg);
	int team_from = GetClientTeam(iClient);
	
	// Allow players always to join spec
	if(team_to == CS_TEAM_SPECTATOR)
	{
		if(Client_HasAdminFlags(iClient, ADMFLAG_BAN) || Client_HasAdminFlags(iClient, ADMFLAG_ROOT))
			EnableObserverMode(iClient);
		else AddClientToQueue(iClient);
		return Plugin_Continue;
	}
	
	if(team_to > CS_TEAM_SPECTATOR && g_bObserve[iClient])
	{
		ToggleObserverMode(iClient);
		return Plugin_Handled;
	}
	
	// Allow to join the same team they are already.
	if (team_from == team_to && team_from != CS_TEAM_NONE)
		return Plugin_Continue;
	
	// Plugin is changing teams
	if(g_bAllowTeamChange[iClient])
		return Plugin_Continue;
	
	// Team has free slots
	if(HasTeamFreeSlot(team_to))
	{
		// Warmup is still active
		if(g_bWaitForPlayers)
		{
			if(GetTeamClientCountFix(InvertTeam(team_to)) == 0 && team_to > CS_TEAM_SPECTATOR)
			{
				MovePlayerToTeam(iClient, InvertTeam(team_to));
				return Plugin_Handled;
			}
			
			return Plugin_Continue;
		}
	}
	
	// Queue player
	MovePlayerToTeam(iClient, CS_TEAM_SPECTATOR);
	AddClientToQueue(iClient);
	
	return Plugin_Handled;
}

void MovePlayerToTeam(int client, int team)
{
	if (GetClientTeam(client) == team)
		return;
	
	g_bAllowTeamChange[client] = true;
	
	if (team > CS_TEAM_SPECTATOR)
	{
		CS_SwitchTeam(client, team);
		CS_UpdateClientModel(client);
	}
	else ChangeClientTeam(client, team);
	
	g_bAllowTeamChange[client] = false;
	
	if(team > CS_TEAM_SPECTATOR)
		CS_UpdateClientModel(client);
}

bool HasTeamFreeSlot(int team)
{
	if(team <= CS_TEAM_SPECTATOR)
		return true;
	
	int iCount;
	LoopIngameClients(client)
	{
		if(GetClientTeam(client) != team)
			continue;
		
		iCount++;
	}
	
	if(team <= CS_TEAM_SPECTATOR)
		return false;
	
	return iCount < (team == CS_TEAM_CT ? g_cvMaxPlayersCT.IntValue : g_cvMaxPlayersT.IntValue);
}

int GetWeekerTeam(bool usePoints)
{
	// Get team player count
	int iCTs, iTs;
	LoopIngameClients(client)
	{
		if(GetClientTeam(client) == CS_TEAM_CT)
			iCTs++;
			
		if(GetClientTeam(client) == CS_TEAM_T)
			iTs++;
	}
	
	// Odd teams, lets fill the one with less players
	if(iCTs != iTs)
	{
		if(iCTs < iTs)
		{
			if(HasTeamFreeSlot(CS_TEAM_CT))
				return CS_TEAM_CT;
		}
		else if(HasTeamFreeSlot(CS_TEAM_T))
			return CS_TEAM_T;
	}
	
	// Teams are equal
	
	// Use points
	if(usePoints)
	{
		int iPointsCT, iPointsT;
		LoopIngameClients(client)
		{
			int team = GetClientTeam(client);
			if(team == CS_TEAM_CT)
				iPointsCT += GetPoints(client);
			else if(team == CS_TEAM_T)
				iPointsT += GetPoints(client);
		}
		
		if(iPointsCT != iPointsT)
		{
			if(iPointsCT < iPointsT)
			{
				if(HasTeamFreeSlot(CS_TEAM_CT))
					return CS_TEAM_CT;
			}
			else if(HasTeamFreeSlot(CS_TEAM_T))
				return CS_TEAM_T;
		}
	}
	
	// Fill CT team first if teams are balanced
	return HasTeamFreeSlot(CS_TEAM_CT) ? CS_TEAM_CT : CS_TEAM_NONE;
}

int GetClientCountEx(bool includequeue)
{
	int counter = 0;
	LoopIngameClients(i)
	{
		if(IsClientSourceTV(i))
			continue;
		
		if(g_bObserve[i])
			continue;
		
		if((includequeue && IsClientInQueue(i)) || GetClientTeam(i) > CS_TEAM_SPECTATOR)
			counter++;
	}
	return counter;
}

int GetTeamClientCountEx(int iTeam, bool bAliveOnly)
{
	int counter = 0;
	LoopIngameClients(i)
	{
		if(GetClientTeam(i) != iTeam)
			continue;
		
		if(bAliveOnly && !IsPlayerAlive(i))
			continue;
		
		counter++;
	}
	return counter;
}

int GetRandomPlayerFromTeam(int team)
{
	int iClients[MAXPLAYERS+1];
	int numClients = 0;

	LoopIngameClients(i)
	{
		if(!IsClientSourceTV(i) && GetClientTeam(i) == team)
		{
			iClients[numClients] = i;
			numClients++;
		}
	}

	return numClients ? iClients[GetRandomInt(0, numClients-1)] : -1;
}