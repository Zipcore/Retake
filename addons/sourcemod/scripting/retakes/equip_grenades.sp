enum GREANDES
{
	GRENADE_HE = 0,
	GRENADE_FLASH1,
	GRENADE_FLASH2,
	GRENADE_SMOKE,
	GRENADE_FIRE,
	GRENADE_DECOY
}

#define GRENADES 6

int g_iMaxGrenades[2][4] = 
{
	// Pistol		Fun		Force	Full	
	{1, 			4, 		1, 		2},		// Terrorists		
	{1,		 	4,		2, 		3}		// Counter-Terrorists
};

float g_fGrenadeMin[GRENADES][2][4] = 
{
	// Terrorists								Counter-Terrorists
	// Pistol		Fun		Force	Full			Pistol	Fun		Force	Full
	{{0.6,		4.0,		0.6,		1.9},			{0.7,		2.1,		2.1,		2.1}},	// He
	{{0.7,		3.0,		0.8,		1.9},			{0.7,		2.6,		2.8,		2.8}},	// Flash 1
	{{0.0,		2.0,		0.2,		0.8},			{0.3,		1.2,		0.9,		0.9}},	// Flash 2
	{{0.3,		2.0,		0.5,		1.7},			{0.5,		1.5,		1.5,		1.5}},	// Smoke
	{{0.1,		2.0,		0.1,		0.9},			{0.0,		1.1,		0.9,		0.9}},	// Fire
	
	{{0.0,		0.0,		0.0,		0.0},			{0.0,		0.0,		0.0,		0.0}}	// Decoy
};

float g_fGrenadeMax[GRENADES][2][4] = 
{
	// Terrorists								Counter-Terrorists
	// Pistol		Fun		Force	Full			Pistol	Fun		Force	Full
	{{1.2,		4.1,		1.9,		3.6},			{1.4,		5.1,		3.9,		4.1}},	// He
	{{1.2,		3.1,		3.1,		4.1},			{1.4,		3.1,		4.9,		5.1}},	// Flash 1
	{{0.0,		2.1,		1.9,		3.1},			{1.2,		3.1,		2.9,		3.9}},	// Flash 2
	{{1.1,		2.1,		1.7,		1.9},			{1.2,		3.1,		2.9,		2.9}},	// Smoke
	{{1.1,		2.1,		1.9,		2.9},			{0.0,		2.1,		2.1,		2.1}},	// Fire
	
	{{0.0,		0.0,		0.0,		0.0},			{0.0,		0.0,		0.0,		0.0}}	// Decoy
};

bool g_bGrenadeEquip[GRENADES][MAXPLAYERS + 1];
int g_iGrenades[MAXPLAYERS + 1];

int g_iFilter[2] = {CLIENTFILTER_TEAMONE, CLIENTFILTER_TEAMTWO};

void SelectGrenades()
{
	// Reset
	LoopClients(iClient)
	{
		for (int iType = 0; iType < GRENADES; iType++)
		{
			g_iGrenades[iClient] = 0;
			g_bGrenadeEquip[iType][iClient] = false;
		}
	}
	
	int iPlayerCount[2];
	
	iPlayerCount[0] = GetTeamClientCountEx(CS_TEAM_T, false);
	iPlayerCount[1] = GetTeamClientCountEx(CS_TEAM_CT, false);
	
	// We need todo this for both teams
	for (int iTeam = 0; iTeam < 2; iTeam++)
	{
		// Loop all grenade types
		for (int iType = 0; iType < GRENADES; iType++)
		{
			float fMin = g_fGrenadeMin[iType][iTeam][g_iRoundType];
			float fMax = g_fGrenadeMax[iType][iTeam][g_iRoundType];
			
			// Use min as max if min is greater than max
			if(fMin > fMax)
				fMax = fMin;
			
			// If min is greater player count reduce it
			while(fMin >= iPlayerCount[iTeam])
			{
				fMin = float(iPlayerCount[iTeam]) - 0.25;
				fMax -= 0.25;
			}
			
			if(fMax > float(iPlayerCount[iTeam]) + 0.1)
				fMax = float(iPlayerCount[iTeam]) + 0.1;
			
			// How much grenades to share on the current team
			float fRandom = GetRandomFloat(fMin, fMax);
			int iCount = RoundToFloor(fRandom);
			
			if(iCount > iPlayerCount[iTeam])
				iCount = iPlayerCount[iTeam];
			
			int ttl = 100;
			while(iCount > 0 && ttl > 0)
			{
				int iClient = Client_GetRandom(g_iFilter[iTeam]);
				
				if(!g_bGrenadeEquip[iType][iClient] && g_iGrenades[iClient] < g_iMaxGrenades[iTeam][g_iRoundType])
				{
					g_bGrenadeEquip[iType][iClient] = true;
					g_iGrenades[iClient]++;
					iCount--;
				}
				
				ttl--;
			}
		}
	}
}

void GiveGrenades(int iClient)
{
	// Loop all grenade types
	for (int iType = 0; iType < GRENADES; iType++)
	{
		if(!g_bGrenadeEquip[iType][iClient])
			continue;
		
		switch(iType)
		{
			case GRENADE_HE: GivePlayerItem(iClient, "weapon_hegrenade");
			case GRENADE_FLASH1: GivePlayerItem(iClient, "weapon_flashbang");
			case GRENADE_FLASH2: GivePlayerItem(iClient, "weapon_flashbang");
			case GRENADE_SMOKE: GivePlayerItem(iClient, "weapon_smokegrenade");
			case GRENADE_FIRE: GetClientTeam(iClient) == CS_TEAM_T ? GivePlayerItem(iClient, "weapon_molotov")  : GivePlayerItem(iClient, "weapon_incgrenade") ;
			case GRENADE_DECOY: GivePlayerItem(iClient, "weapon_decoy");
		}
		
		g_bGrenadeEquip[iType][iClient] = false;
	}
}