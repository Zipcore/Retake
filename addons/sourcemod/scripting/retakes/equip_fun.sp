char g_sDoom[][] = {
	"{darkred}DOOOOOOOOOOOOMMMMM",
	"{bluegrey}The CAKE is a LIE !ô!",
	"{grey}Trol01O1O10lo",
	"{orange}Don't say nobody warned you about this.",
	"{lime}I{purple}t{lime}'{purple}s{lime} a{orange} T{lime}r{purple}a{lime}p{purple}! {darkred}@dd{orange}m0re",
	"{orange}Mortal Combat",
	"{grey}All Random!",
	"{bluegrey}Entering new test ch{grey}$/&(%&{darkblue}???",
	"{lime}Easy {bluegrey}Mode",
	"{bluegrey}Entering new test chamber.",
	"{bluegrey}Entering new test chamber.",
	"{bluegrey}Entering new test chamber.",
	"{bluegrey}Entering new test chamber.",
	"{bluegrey}Entering new test chamber."
};

int Fun_Equip(int iClient, int iTeam)
{
	// Give Kevlar-Helm
	SetEntProp(iClient, Prop_Data, "m_ArmorValue", 100);
	SetEntData(iClient, g_iHelmetOffset, 1);
	
	//Fun_BombMode(iClient);
	
	return Fun_Weapons(iClient, iTeam);
}
//g_iFunMode
int Fun_Weapons(int iClient, int iTeam)
{
	// Deagle + Scout
	if (g_iFunMode < 30)
	{
		GivePlayerItem(iClient, "weapon_deagle");
		return GivePlayerItem(iClient, "weapon_ssg08");
	}
	
	// Revolver + Scout
	if (g_iFunMode < 35)
	{
		GivePlayerItem(iClient, "weapon_revolver");
		return GivePlayerItem(iClient, "weapon_ssg08");
	}
	
	// Deagle + Grenades + Taser
	if (g_iFunMode < 50)
	{
		GivePlayerItem(iClient, "weapon_taser");
		return GivePlayerItem(iClient, "weapon_deagle");
	}
	
	// MP
	if (g_iFunMode < 60)
	{
		if(GetRandomInt(1, 100) <= 50)
		{
			GivePrefencePistol(iClient, iTeam);
		}
		else 
		{
			GiveDefaultPistol(iClient, iTeam);
		}
		
		return GiveMP(iClient);
	}
	
	// Shotgun
	if (g_iFunMode < 70)
	{
		GiveDefaultPistol(iClient, iTeam);
		
		return GiveShotgun(iClient);
	}
	
	
	// Shotgun & MP
	if (g_iFunMode < 85)
	{
		GiveDefaultPistol(iClient, iTeam);
		
		if(GetRandomInt(1, 100) <= 50)
			return GiveMP(iClient);
		
		return GiveShotgun(iClient);
	}
	
	// AWP + Deagle + Taser
	
	if (g_iFunMode < 95)
	{
		GivePlayerItem(iClient, "weapon_deagle");
		GivePlayerItem(iClient, "weapon_taser");
		return GivePlayerItem(iClient, "weapon_awp");
	}
	
	// Default: Grenades + Strong pistols
	
	return GivePrefencePistol(iClient, iTeam);
}