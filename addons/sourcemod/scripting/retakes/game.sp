Handle g_hTimerCheckPlayers = null;
int g_iCountdown;

void StartPlayerCheckTimer()
{
	if(g_hTimerCheckPlayers != null)
		delete g_hTimerCheckPlayers;
	
	g_bWaitForPlayers = true;
	g_iCountdown = 30;
	WaitForPlayers();
	
	g_hTimerCheckPlayers = CreateTimer(1.0, Timer_CheckPlayers, _, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
}

public Action Timer_CheckPlayers(Handle timer, any data)
{
	if(g_bEditMode)
		return Plugin_Continue;
	
	int iCount;
	LoopIngameClients(iClient)
	{
		if(GetClientTeam(iClient) == CS_TEAM_NONE)
			continue;
		
		iCount++;
	}
	
	if(iCount < 2)
	{
		PrintHintTextToAll("\n<font size='22' color='#FE2E2E'>Waiting for players to join</font>");
		if(!g_bWaitForPlayers)
		{
			g_bWaitForPlayers = true;
			WaitForPlayers();
		}
		if(g_iCountdown < 10)
			g_iCountdown = 10;
	}
	else if(g_bWaitForPlayers)
	{
		if(g_iCountdown > 1)
		{
			g_iCountdown--;
			PrintHintTextToAll("\n<font size='22'>Retakes will start in: </font><font size='22' color='#FE2E2E'>%d seconds.</font>", g_iCountdown);
			
			LoopIngameClients(iClient)
			{
				if(GetClientTeam(iClient) > CS_TEAM_SPECTATOR && !IsPlayerAlive(iClient))
					CS_RespawnPlayer(iClient);
			}
			
			return Plugin_Continue;
		}
		
		g_iCountdown = 0;
		
		g_bWaitForPlayers = false;
		
		StartGame();
	}
	
	return Plugin_Continue;
}

void StartGame()
{
	ServerCommand("mp_restartgame 1");
	ServerCommand("mp_warmup_end");
		
	ServerCommand("exec retakes.cfg");
	ServerCommand("exec retakes_live.cfg");
	
	ServerCommand("mp_defuser_allocation 0");
		
	ScrambleTeamsOnNextUpdate();
	UpdateTeams();
}

void WaitForPlayers()
{
	ServerCommand("exec retakes.cfg");
	ServerCommand("exec retakes_warmup.cfg");
}