void EquipForce(int iClient)
{
	int iHelmet;
	int iArmor;
	
	float fPrimary;
	
	switch(g_iWinRowCounter)
	{
		case 1:
		{
			iArmor = 80;
			iHelmet = GetRandomInt(0, 1);
			
			fPrimary = GetRandomFloat(0.25, 1.0);
		}
		case 2:
		{
			iArmor = 60;
			
			fPrimary = GetRandomFloat(0.0, 0.8);
		}
		default:
		{
			iArmor = 50;
		}
	}
	
	SetEntProp(iClient, Prop_Data, "m_ArmorValue", iArmor);
	SetEntData(iClient, g_iHelmetOffset, iHelmet);
	
	if(fPrimary >= 0.93)
		GivePlayerItem(iClient, "weapon_ssg08");
	else if(fPrimary >= 0.85)
		GivePlayerItem(iClient, "weapon_galilar");
	else if(fPrimary >= 0.7)
		GivePlayerItem(iClient, "weapon_ump45");
	else if(fPrimary >= 0.55)
		GivePlayerItem(iClient, "weapon_mp7");
	else if(fPrimary >= 0.52)
		GivePlayerItem(iClient, "weapon_bizon");
	else if(fPrimary >= 0.25)
		GivePlayerItem(iClient, "weapon_mac10");
	
	GivePrefencePistol(iClient, CS_TEAM_T);
}