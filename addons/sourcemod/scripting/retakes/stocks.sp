#define LoopClients(%1) for(int %1 = 1; %1 <= MaxClients; %1++)

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))

#define LoopIngamePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && !IsFakeClient(%1))

#define LoopAlivePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && IsPlayerAlive(%1))

#define LoopArray(%1,%2) for(int %1=0;%1<GetArraySize(%2);++%1)

stock int InvertTeam(int team)
{
	return team == CS_TEAM_CT ? CS_TEAM_T : CS_TEAM_CT;
}

stock int GetTeamClientCountFix(int team)
{
	int count = 0;
	LoopIngameClients(i)
	{
		if(GetClientTeam(i) == team)
			count++;
	}
	return count;
}

stock int GetTeamAliveClientCount(int team)
{
	int count = 0;
	LoopIngameClients(i)
	{
		if(GetClientTeam(i) == team && IsPlayerAlive(i))
			count++;
	}
	return count;
}

stock void Fade(int iClient, int duration, int hold, int flags, bool fun)
{
	Handle message = StartMessageOne("Fade", iClient, USERMSG_RELIABLE);
	PbSetInt(message, "duration", duration);
	PbSetInt(message, "hold_time", hold);
	PbSetInt(message, "flags", flags);
	
	if(iClient == g_iBomber)
		PbSetColor(message, "clr", { 255, 120, 66, 100 });
	else PbSetColor(message, "clr", GetClientTeam(iClient) == CS_TEAM_CT ?  { 15, 15, 255, 50 }  :  { 255, 15, 15, 50 });
	
	EndMessage();
}

/*
stock void PrintToConsoleAll(char[] msg, any ...)
{
	char sFormat[512];
	VFormat(sFormat, sizeof(sFormat), msg, 2);
	
	LoopIngamePlayers(i)
	{
		PrintToConsole(i, sFormat);
	}
}*/