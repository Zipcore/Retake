void RegisterAdminCommands()
{
	RegAdminCmd("sm_start", Command_Start, ADMFLAG_ROOT);
	RegAdminCmd("sm_scramble", Command_Scramble, ADMFLAG_ROOT);
	RegAdminCmd("sm_del", Command_DeleteSpawn, ADMFLAG_ROOT);
	RegAdminCmd("sm_delete", Command_DeleteSpawn, ADMFLAG_ROOT);
	RegAdminCmd("sm_delall", Command_DeleteSpawnAll, ADMFLAG_ROOT);
	RegAdminCmd("sm_delete_all", Command_DeleteSpawnAll, ADMFLAG_ROOT);
	RegAdminCmd("sm_delnear", Command_DeleteSpawnNear, ADMFLAG_ROOT);
	RegAdminCmd("sm_infonear", Command_InfoSpawnNear, ADMFLAG_ROOT);
	RegAdminCmd("sm_delete_near", Command_DeleteSpawnNear, ADMFLAG_ROOT);
	RegAdminCmd("sm_add", Command_AddSpawn, ADMFLAG_ROOT);
	RegAdminCmd("sm_edit", Command_Edit, ADMFLAG_ROOT);
	RegAdminCmd("sm_spawns", Command_Spawns, ADMFLAG_ROOT);
	RegAdminCmd("sm_givec4", Command_GiveC4, ADMFLAG_ROOT);
	RegAdminCmd("sm_importspawns", Command_ImportSpawns, ADMFLAG_ROOT);
}

public Action Command_Start(int client, int args)
{
	g_fMapLoadTime -= 50;
	
	if(g_hMapLoadTimer != null)
		TriggerTimer(g_hMapLoadTimer, false);
	else ReplyToCommand(client, "Warmup has already ended.");
	
	return Plugin_Handled;
}

public Action Command_Scramble(int client, int args)
{
	g_bScrambling = true;
	return Plugin_Handled;
}

public Action Command_GiveC4(int client, int args)
{
	GivePlayerItem(client, "weapon_c4");
	return Plugin_Handled;
}

public Action Command_Spawns(int client, int args)
{
	int aCt, bCt;
	int aT, bT;
	int aB, bB;
	for (int i = 0; i < g_SpawnsCount; i++)
	{
		if(g_Spawns[i][Type] == Ct && g_Spawns[i][Site] == SITEA)
			aCt++;
		if(g_Spawns[i][Type] == Ct && g_Spawns[i][Site] == SITEB)
			bCt++;
		if(g_Spawns[i][Type] == T && g_Spawns[i][Site] == SITEA)
			aT++;
		if(g_Spawns[i][Type] == T && g_Spawns[i][Site] == SITEB)
			bT++;
		if(g_Spawns[i][Type] == Bomb && g_Spawns[i][Site] == SITEA)
			aB++;
		if(g_Spawns[i][Type] == Bomb && g_Spawns[i][Site] == SITEB)
			bB++;
	}
	CPrintToChatAll("A: CT: %d, T: %d, Bomb: %d", aCt, aT, aB);
	CPrintToChatAll("B: CT: %d, T: %d, Bomb: %d", bCt, bT, bB);
	return Plugin_Handled;
}

public Action Command_Edit(int client, int args)
{
	g_bEditMode = !g_bEditMode;
	CPrintToChatAll("%sEdit mode is now %s.", PREFIX, g_bEditMode ? "Enabled":"Disabled");
	if(g_bEditMode)
		CreateTimer(0.1, DrawSpawns, _, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
	
	ServerCommand("mp_restartgame 1");
	return Plugin_Handled;
}

public Action Command_DeleteSpawnNear(int client, int args)
{
	float pos[3];
	GetClientAbsOrigin(client, pos);
	
	float dist = -1.0;
	int SpawnNum = -1;
	for (int i = 0; i < g_SpawnsCount; i++)
	{
		float pos2[3];
		Array_Copy(g_Spawns[i][Location], pos2, 3);
		float dist2 = GetVectorDistance(pos2, pos);
		
		if(dist == -1.0 || dist > dist2)
		{
			dist = dist2;
			SpawnNum = i;
		}
	}

	
	if(SpawnNum != -1)
	{
		PrintToChat(client, "Deleted spawnpoint, which was %.2f units away from your position.", dist);
		SQL_DeleteSpawn(client, SpawnNum);
		SQL_LoadSpawns();
	}
	
	return Plugin_Handled;
}

public Action Command_InfoSpawnNear(int client, int args)
{
	float pos[3];
	GetClientAbsOrigin(client, pos);
	
	int SpawnNum = -1;
	int iCount;
	for (int i = 0; i < g_SpawnsCount; i++)
	{
		float pos2[3];
		Array_Copy(g_Spawns[i][Location], pos2, 3);
		float dist = GetVectorDistance(pos2, pos);
		
		if(dist < 512.0)
		{
			iCount++;
			PrintToChat(client, "Distance: %.2f - Pos: %.2f|%.2f|%.2f - Type: %s - Site: %s", 
				dist, pos2[0], pos2[1], pos2[2], 
				g_Spawns[SpawnNum][Type] == Bomb ? "Bomb" : (g_Spawns[SpawnNum][Type] == Ct ? "CT" : ((g_Spawns[SpawnNum][Type] == T ? "T" : "None"))),
				g_Spawns[SpawnNum][Site] == 0 ? "A" : "B"
			);
		}
	}
	
	PrintToChat(client, "Fiund %i spawn points within 512 units.", iCount);
	
	return Plugin_Handled;
}

public Action Command_DeleteSpawnAll(int client, int args)
{
	Handle menu = CreateMenu(MenuHandler_DeleteAll);
	SetMenuTitle(menu, "Delete ALL spawns ?!? U mad :3");
	AddMenuItem(menu, "0", "No, forgive me!");
	AddMenuItem(menu, "0", "What is this?");
	AddMenuItem(menu, "1", "I know what I'm doing");
	AddMenuItem(menu, "0", "I like trains");
	AddMenuItem(menu, "2", "Kill me");
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}

public int MenuHandler_DeleteAll(Handle menu, MenuAction action, int client, int info)
{
	if (action == MenuAction_Select)
	{
		char sInfo[32];		
		GetMenuItem(menu, info, sInfo, sizeof(sInfo));
		
		int x = StringToInt(sInfo);
		if(x == 2)
			ForcePlayerSuicide(client);
		else if(x == 1)
		{
			PrintToChat(client, "Deleted %d spawns", g_SpawnsCount);
			
			for (int i = 0; i < g_SpawnsCount; i++)
				SQL_DeleteSpawn(client, i);
			SQL_LoadSpawns();
		}
		else PrintToChat(client, "Nothing happend...");
	}
	else if (action == MenuAction_End)
		CloseHandle(menu);
}

public Action Command_DeleteSpawn(int client, int args)
{
	if(g_SpawnsCount > 0)
	{
		Handle hmenu = CreateMenu(MenuHandler_Teleport);
		SetMenuTitle(hmenu, "Select Spawn");
		
		char sDisplay[64];
		char sInfo[64];
		
		for (int i = 0; i < g_SpawnsCount; i++)
		{
			FormatEx(sDisplay, sizeof(sDisplay), "Site:%s, Type:", g_Spawns[i][Site] == SITEA ? "A":"B");
			if(g_Spawns[i][Type] == Bomb)
				FormatEx(sDisplay, sizeof(sDisplay), "%sBomb", sDisplay);
			else if(g_Spawns[i][Type] == T)
				FormatEx(sDisplay, sizeof(sDisplay), "%sT", sDisplay);
			else if(g_Spawns[i][Type] == Ct)
				FormatEx(sDisplay, sizeof(sDisplay), "%sCt", sDisplay);
			FormatEx(sInfo, sizeof(sInfo), "%d", i);
			AddMenuItem(hmenu, sInfo, sDisplay);
		}
		
		SetMenuExitButton(hmenu, true);
		DisplayMenu(hmenu, client, MENU_TIME_FOREVER);
	}
	
	return Plugin_Handled;
}

public int MenuHandler_Teleport(Handle menu, MenuAction action, int client, int info)
{
	if (action == MenuAction_Select)
	{
		char sInfo[32];		
		GetMenuItem(menu, info, sInfo, sizeof(sInfo));
		int spawnNum = StringToInt(sInfo);
		float loc[3];
		float ang[3];
		Array_Copy(g_Spawns[spawnNum][Location], loc, 3);
		Array_Copy(g_Spawns[spawnNum][Angles], ang, 3);
		TeleportEntity(client, loc, ang, NULL_VECTOR);
		Handle hmenu = CreateMenu(MenuHandler_Delete);
		SetMenuTitle(hmenu, "Delete this spawn:");
		FormatEx(sInfo, sizeof(sInfo), "%d", g_Spawns[spawnNum][Id]);
		AddMenuItem(hmenu, sInfo, "Yes");
		AddMenuItem(hmenu, "-1", "No");
		DisplayMenu(hmenu, client, MENU_TIME_FOREVER);
	}
	else if (action == MenuAction_End)
		CloseHandle(menu);
}

public int MenuHandler_Delete(Handle menu, MenuAction action, int client, int info)
{
	if (action == MenuAction_Select)
	{
		char sInfo[32];		
		GetMenuItem(menu, info, sInfo, sizeof(sInfo));
		
		int spawnNum = StringToInt(sInfo);
		if(spawnNum == -1)
			Command_DeleteSpawn(client, 0);
		else 
		{
			SQL_DeleteSpawn(client, spawnNum);
			SQL_LoadSpawns();
		}
	}
	else if (action == MenuAction_End)
		CloseHandle(menu);
}

public Action Command_AddSpawn(int client, int args)
{
	Handle menu = CreateMenu(MenuHandler_Site, MENU_ACTIONS_ALL);
	SetMenuTitle(menu, "Choose Site:");
	
	AddMenuItem(menu, "0", "A");
	AddMenuItem(menu, "1", "B");
	
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}

public int MenuHandler_Site(Handle menu, MenuAction action, int iClient, int iInfo)
{
	if(action == MenuAction_Select)
	{
		char sArg[6];
		GetMenuItem(menu, iInfo, sArg, sizeof(sArg));
		g_iChoosedSite[iClient] = StringToInt(sArg);
		Menu_AddSpawnType(iClient);
	}
	else if (action == MenuAction_End)
		CloseHandle(menu);
}

void Menu_AddSpawnType(int iClient)
{
	Handle menu = CreateMenu(MenuHandler_Type, MENU_ACTIONS_ALL);
	SetMenuTitle(menu, "Choose Spawn Type:");
	
	AddMenuItem(menu, "1", "Bomber");
	AddMenuItem(menu, "2", "T");
	AddMenuItem(menu, "3", "CT");
	
	SetMenuExitBackButton(menu, true);
	DisplayMenu(menu, iClient, MENU_TIME_FOREVER);
}

public int MenuHandler_Type(Handle menu, MenuAction action, int iClient, int iInfo)
{
	if(action == MenuAction_Select)
	{
		char sInfo[6];
		GetMenuItem(menu, iInfo, sInfo, sizeof(sInfo));
		
		int iType = StringToInt(sInfo);
		/*
		if(iType == 1 && !GetEntProp(iClient, Prop_Send, "m_bInBombZone"))
		{
			Menu_AddSpawnType(iClient);
			CPrintToChat(iClient, "%sYou're not inside a bomb zone.", PREFIX);
		}
		else SQL_AddSpawn(iClient, iType);
		*/
		
		SQL_AddSpawn(iClient, iType);
	}
	else if(action == MenuAction_Cancel)
		Command_AddSpawn(iClient, 0);
	else if (action == MenuAction_End)
		CloseHandle(menu);
}

public Action DrawSpawns(Handle timer)
{
	if (!g_bEditMode)
		return Plugin_Stop;
	
	int g_DrawColor[4];
	float Point1[3];
	float Point2[3];
	for (int i = 0; i < g_SpawnsCount; i++)
	{
		//Set Color
		if(g_Spawns[i][Type] == Ct)
		{
			g_DrawColor[0] = 0;
			g_DrawColor[1] = 255;
			g_DrawColor[2] = 255;
		}
		else if (g_Spawns[i][Type] == T)
		{
			g_DrawColor[0] = 255;
			g_DrawColor[1] = 0;
			g_DrawColor[2] = 0;
		}
		else if (g_Spawns[i][Type] == Bomb)
		{
			g_DrawColor[0] = 255;
			g_DrawColor[1] = 255;
			g_DrawColor[2] = 0;
		}
		g_DrawColor[3] = 255;
		//Set Points
		Array_Copy(g_Spawns[i][Location], Point1, 3);
		Array_Copy(Point1, Point2, 3);
		Point2[2] += 100.0;
		//Draw Beam
		TE_SetupBeamPoints(Point1, Point2, g_precacheLaser, 0, 0, 0, 0.1, 3.0, 3.0, 10, 0.0, g_DrawColor, 0);TE_SendToAll(0.0);
		//Draw Light Ball if site == A
		if(g_Spawns[i][Site] == SITEA)
		{
			Point2[0] = 0.0;
			Point2[1] = 0.0;
			Point2[2] = 0.0;
			TE_SetupGlowSprite(Point1, g_precacheGlow, 0.1, 1.0, 255);TE_SendToAll(0.0);
		}
	}
	
	return Plugin_Continue;
}

stock void GetCleanMapName(char[] buffer, int size) 
{
	char mapName[PLATFORM_MAX_PATH+1];
	GetCurrentMap(mapName, sizeof(mapName));
	int last_slash = 0;
	int len = strlen(mapName);
	
	for (int i = 0;  i < len; i++) 
		if (mapName[i] == '/')
			last_slash = i + 1;
	
	strcopy(buffer, size, mapName[last_slash]);
}

void GetConfigFileName(char[] buffer, int size) 
{
    char mapName[128];
    GetCleanMapName(mapName, sizeof(mapName));
    BuildPath(Path_SM, buffer, size, "configs/retakes/%s.cfg", mapName);
}

public Action Command_ImportSpawns(int client, int args)
{
	char configFile[PLATFORM_MAX_PATH];
	GetConfigFileName(configFile, sizeof(configFile));
	
	if (!FileExists(configFile)) {
		ReplyToCommand(client, "The retakes config file (%s) does not exist", configFile);
		return Plugin_Handled;
	}
	
	KeyValues kv = new KeyValues("Spawns");
	if (!kv.ImportFromFile(configFile) || !kv.GotoFirstSubKey()) 
	{
		ReplyToCommand(client, "The retakes config file was empty");
		delete kv;
		return Plugin_Handled;
	}
	
	int iSpawns = 0;
	int iBomb = 0;
	
	do {
		char sBuffer[32];
		kv.GetSectionName(sBuffer, sizeof(sBuffer));
		
		float fVec[3];
		kv.GetVector("origin", fVec, NULL_VECTOR);
		float fAng[3];
		kv.GetVector("angle", fAng, NULL_VECTOR);
		
		kv.GetString("bombsite", sBuffer, sizeof(sBuffer), "A");
		int iSite = StrEqual(sBuffer, "A") ? 0 : 1;
		
		kv.GetString("team", sBuffer, sizeof(sBuffer), "T");
		int iType = StrEqual(sBuffer, "CT") ? 3 : 2;
		
		if(kv.GetNum("type") == 1)
		{
			iType = 1;
			iBomb++;
		}
		
		SQL_AddSpawnEx(client, iType, fVec, fAng, iSite);
		
		iSpawns++;
	
	} while (kv.GotoNextKey());
	
	delete kv;
	
	ReplyToCommand(client, "Imported %i spawns for the current map, including %i bomber spawns. reloading spawns from sql now.", iSpawns, iBomb);
	
	SQL_LoadSpawns();
	
	return Plugin_Handled;
}