void RegisterGunMenuCommands()
{
	RegConsoleCmd("sm_guns", Command_Guns);
	RegConsoleCmd("sm_awp", Command_Guns);
	RegConsoleCmd("sm_m4", Command_Guns);
	RegConsoleCmd("sm_m4a1", Command_Guns);
	RegConsoleCmd("sm_m4a4", Command_Guns);
	
	RegConsoleCmd("sm_ob", Command_Observe);
	RegConsoleCmd("sm_observe", Command_Observe);
	
	AddCommandListener(Command_Say, "say");
	AddCommandListener(Command_Say, "say_team");
}

public Action Command_Observe(int iClient, int args)
{
	ToggleObserverMode(iClient);
	
	return Plugin_Handled;
}

public Action Command_Guns(int iClient, int args)
{
	Menu_Guns(iClient);
	
	return Plugin_Handled;
}

public Action Command_Say(int iClient, const char[] command, int argc)
{
	char sText[192];
	GetCmdArgString(sText, sizeof(sText));
	StripQuotes(sText);
	if(StrEqual(sText, "guns") || StrEqual(sText, "weapons") || StrEqual(sText, "weps"))
	{
		Command_Guns(iClient, 0);
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

void Menu_Guns(int iClient)
{
	Handle menu = CreateMenu(MenuHandler_Guns, MENU_ACTIONS_ALL);
	SetMenuTitle(menu, "Choose your Weapons(t/ct):");
	
	AddMenuItem(menu, "m4a4", "AK47 & M4A4");
	AddMenuItem(menu, "m4a1", "AK47 & M4A1-S");
	AddMenuItem(menu, "famas", "Galil & Famas");
	if(g_cvAugVipOnly.IntValue == 0 || Client_HasAdminFlags(iClient, ADMFLAG_RESERVATION))
		AddMenuItem(menu, "aug", "SG553 & Aug");
	AddMenuItem(menu, "ssg", "SSG08");
	
	DisplayMenu(menu, iClient, MENU_TIME_FOREVER);
}

public MenuHandler_Guns(Handle menu, MenuAction action, int iClient, int info)
{
	if(action == MenuAction_Select)
	{
		char sBuffer[64];
		GetMenuItem(menu, info, sBuffer, sizeof(sBuffer));
		Pref_SetPrimary(iClient, sBuffer);
		
		if(g_cvAwpPerPlayerChance.IntValue > 0)
			Menu_AWP(iClient);
		else Menu_Pistol(iClient);
	}
	else if(action == MenuAction_End)
		CloseHandle(menu);
}

void Menu_AWP(int iClient)
{
	Handle menu = CreateMenu(MenuHandler_Awp, MENU_ACTIONS_ALL);
	SetMenuTitle(menu, "Do you want to play with AWP?");
	
	AddMenuItem(menu, "1", "Always");
	AddMenuItem(menu, "2", "Sometimes");
	AddMenuItem(menu, "0", "Never");
	
	DisplayMenu(menu, iClient, MENU_TIME_FOREVER);
}

public MenuHandler_Awp(Handle menu, MenuAction action, int iClient, int info)
{
	if(action == MenuAction_Select)
	{
		char sInfo[64];
		GetMenuItem(menu, info, sInfo, sizeof(sInfo));
		Pref_SetAWP(iClient, sInfo);
		
		Menu_Pistol(iClient);
	}
	else if(action == MenuAction_End)
		CloseHandle(menu);
}

void Menu_Pistol(int iClient)
{
	Handle menu = CreateMenu(MenuHandler_Pistol, MENU_ACTIONS_ALL);
	SetMenuTitle(menu, "Which pistol you want?");
	
	AddMenuItem(menu, "0", "P2000 / Glock");
	
	if(g_cvStrongPistolChance.IntValue == 100)
		AddMenuItem(menu, "1", "Five Seven / Tec9");
	else if(g_cvStrongPistolChance.IntValue > 0)
		AddMenuItem(menu, "1", "Five Seven / Tec9 / P250");
	
	if(g_cvStrongPistolChance.IntValue > 0)
		AddMenuItem(menu, "3", "Deagle / Five Seven / Tec9 / P250");
		
	if(g_cvStrongPistolChance.IntValue > 0)
		AddMenuItem(menu, "4", "Deagle / CZ-75 / P250");
	
	AddMenuItem(menu, "2", "P250");
	
	DisplayMenu(menu, iClient, MENU_TIME_FOREVER);
}

public MenuHandler_Pistol(Handle menu, MenuAction action, int iClient, int info)
{
	if(action == MenuAction_Select)
	{
		char sInfo[64];
		GetMenuItem(menu, info, sInfo, sizeof(sInfo));
		Pref_SetPistol(iClient, sInfo);
		
		Menu_HUD(iClient);
	}
	else if(action == MenuAction_End)
		CloseHandle(menu);
}

void Menu_HUD(int iClient)
{
	Handle menu = CreateMenu(MenuHandler_HUD, MENU_ACTIONS_ALL);
	SetMenuTitle(menu, "Select you HUD mode?");
	
	AddMenuItem(menu, "1", "Under radar");
	AddMenuItem(menu, "2", "Bottom center");
	AddMenuItem(menu, "3", "Both");
	AddMenuItem(menu, "0", "No HUD");
	
	DisplayMenu(menu, iClient, MENU_TIME_FOREVER);
}

public MenuHandler_HUD(Handle menu, MenuAction action, int iClient, int info)
{
	if(action == MenuAction_Select)
	{
		char sInfo[64];
		GetMenuItem(menu, info, sInfo, sizeof(sInfo));
		Pref_SetHUD(iClient, sInfo);
		
		Menu_Sounds(iClient);
	}
	else if(action == MenuAction_End)
		CloseHandle(menu);
}

void Menu_Sounds(int iClient)
{
	Handle menu = CreateMenu(MenuHandler_Sounds, MENU_ACTIONS_ALL);
	SetMenuTitle(menu, "Do you like to hear round start sounds?");
	
	AddMenuItem(menu, "1.0", "Yes (Volume = 100%)");
	AddMenuItem(menu, "0.8", "Yes (Volume = 80%)");
	AddMenuItem(menu, "0.6", "Yes (Volume = 60%)");
	AddMenuItem(menu, "0.4", "Yes (Volume = 40%)");
	AddMenuItem(menu, "0.2", "Yes (Volume = 20%)");
	AddMenuItem(menu, "0.0", "No, thanks");
	
	DisplayMenu(menu, iClient, MENU_TIME_FOREVER);
}

public MenuHandler_Sounds(Handle menu, MenuAction action, int iClient, int info)
{
	if(action == MenuAction_Select)
	{
		char sInfo[64];
		GetMenuItem(menu, info, sInfo, sizeof(sInfo));
		Pref_SetVolume(iClient, sInfo);
		
		Credits(iClient);
	}
	else if(action == MenuAction_End)
		CloseHandle(menu);
}

void Credits(int iClient)
{
	CPrintToChat(iClient, "%sRetake version %s", PREFIX, PL_VERSION);
	CPrintToChat(iClient, "%s3kliksphilip.com edition by .#Zipcore", PREFIX);
}