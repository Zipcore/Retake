public Action MsgHook_TextMsg(UserMsg:msg_id, Handle msg, const players[], playersNum, bool reliable, bool init)
{
	char sBuffer[64];
	PbReadString(msg, "params", sBuffer, sizeof(sBuffer), 0);
	
	if(StrContains(sBuffer, "Cash_Award") != -1)
		return Plugin_Handled;
	else if(StrContains(sBuffer, "Point_Award") != -1)
		return Plugin_Handled;
	else if(StrContains(sBuffer, "SavePlayer") != -1)
		return Plugin_Handled;
	else if(StrContains(sBuffer, "Bomb") != -1)
		return Plugin_Handled;
	else if(StrContains(sBuffer, "C4") != -1)
		return Plugin_Handled;
		
	return Plugin_Continue;
}