public Action CS_OnCSWeaponDrop(int iClient, int iWeapon) 
{
	if(!g_bEditMode && iClient == g_iBomber && iWeapon == g_iBomb && g_cvForcePlant.BoolValue && !g_bBombPlanted)
		return Plugin_Stop;
	return Plugin_Continue;
}

public Action Hook_WeaponCanSwitch(int iClient, int iWeapon) 
{
	if(!g_bEditMode && iClient == g_iBomber && g_cvForcePlant.BoolValue && iWeapon != g_iBomb && !g_bBombPlanted)
		return Plugin_Handled;

	return Plugin_Continue;
}

public Action Event_OnBombDefused(Handle event, const char[] name, bool dontBroadcast)
{
	int defuser = GetClientOfUserId(GetEventInt(event, "userid"));
	
	int iAliveTs = GetTeamAliveClientCount(CS_TEAM_T);
	
	if(iAliveTs > 0)
	{
		CPrintToChatAll("%s{purple}NINJA DEFUSE", PREFIX);
		AddPoints(defuser, 75*iAliveTs, "+ %i points for ninja defusing the bomb", 75*iAliveTs);
		
		LoopIngameClients(i)
		{
			if(GetClientTeam(i) != CS_TEAM_T)
				continue;
			
			if(!IsPlayerAlive(i))
				continue;
			
			AddPoints(i, -100, "- %i points for being alive while the bomb got defused", 100);
		}
	}
	else AddPoints(defuser, 50, "+ %i points for defusing the bomb", 50);
	
	return Plugin_Continue;
}

public Action Event_OnBeginPlant(Handle event, const char[] name, bool dontBroadcast)
{
	if(g_cvInstantPlant.BoolValue)
		CreateTimer(0.0, Timer_InstantPlant, GetEventInt(event, "userid"));
	
	return Plugin_Continue;
}

public Action Event_OnBombPlanted(Handle event, const char[] name, bool dontBroadcast)
{
	g_bBombPlanted = true;
	
	if(g_iBomber > 0 && IsClientInGame(g_iBomber))
	{
		GetClientAbsOrigin(g_iBomber, g_fBombPos);
		GiveEquipment(g_iBomber);
	}
	
	return Plugin_Continue;
}

public Action Timer_InstantPlant(Handle timer, any userid)
{
	int client = GetClientOfUserId(userid);
	
	if(client != 0 && IsPlayerAlive(client))
	{
		int c4 = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
		
		char classname[30];
		GetEntityClassname(c4, classname, sizeof(classname));
		
		if(StrEqual(classname, "weapon_c4", false))
			SetEntPropFloat(c4, Prop_Send, "m_fArmedTime", GetGameTime());
	}
	
	return Plugin_Continue;
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if(!g_bEditMode && g_cvForcePlant.BoolValue && g_iWepType[client] == WEAPONTYPE_C4)
	{
		ClientCommand(client, "slot%d", CS_SLOT_C4+1);
		
		buttons |= IN_USE;
		return Plugin_Changed;
	}
  	
  	return Plugin_Continue;
}