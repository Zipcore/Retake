void ResetEquipment(int iClient)
{
	SetEntData(iClient, g_iMoneyOffset, 0);
	
	SetEntProp(iClient, Prop_Data, "m_ArmorValue", 0);
	SetEntData(iClient, g_iHelmetOffset, 0);
	
	Client_RemoveAllWeapons(iClient);
}

void GiveEquipment(int iClient)
{
	if(GetClientMenu(iClient))
		CancelClientMenu(iClient, true);
	
	int iTeam = GetClientTeam(iClient);
	
	int weapon = GivePlayerItem(iClient, "weapon_knife");
	
	// Defuse KIT
	if(g_iRoundType != RT_FORCE || g_cvDefuseKit.IntValue == 0 || GetRandomInt(0, 100) <= g_cvDefuseKitChance.IntValue || (g_cvDefuseKit.IntValue == 2 && Client_HasAdminFlags(iClient, ADMFLAG_RESERVATION)))
		SetEntProp(iClient, Prop_Send, "m_bHasDefuser", 1);
	
	// Give Taser
	if(GetRandomInt(1, 100) <= g_cvTaserChance.IntValue)
		weapon = GivePlayerItem(iClient, "weapon_taser");
	
	// Pistol Round
	if(g_iRoundType == RT_PISTOL)
	{
		// Give Kevlar
		SetEntProp(iClient, Prop_Data, "m_ArmorValue", 100);
		SetEntData(iClient, g_iHelmetOffset, 0);
		
		// Give default pistol
		weapon = GiveDefaultPistol(iClient, iTeam);
	}
	
	// FUN Round
	else if(g_iRoundType == RT_FUN)
	{
		g_iFunMode = GetRandomInt(1, 100);
		weapon = Fun_Equip(iClient, iTeam);
	}
	else if(g_iRoundType == RT_FORCE)
	{
		if(iTeam == CS_TEAM_T)
		{
			EquipForce(iClient);
		}
		else
		{
			// Give Kevlar
			SetEntProp(iClient, Prop_Data, "m_ArmorValue", 100);
			SetEntData(iClient, g_iHelmetOffset, 1);
			
			// Give Primary Weapon
			weapon = GivePrefencePistol(iClient, iTeam);
			
			// Give AWP
			if(g_iAwpCt == iClient && GetRandomInt(0, 100) > 65)
				weapon = GivePlayerItem(iClient, "weapon_awp");
			// Give Primary Weapon
			else weapon = GivePrimaryWeapon(iClient, iTeam);
		}
	}
	else
	{
		// Give Kevlar+Helmet
		SetEntProp(iClient, Prop_Data, "m_ArmorValue", 100);
		SetEntData(iClient, g_iHelmetOffset, 1);
			
		// Give Prefence Pistol
		weapon = GivePrefencePistol(iClient, iTeam);
		
		// Give AWP
		if(g_iAwpCt == iClient || g_iAwpT == iClient)
			weapon = GivePlayerItem(iClient, "weapon_awp");
		// Give Primary Weapon
		else weapon = GivePrimaryWeapon(iClient, iTeam);
	}
	
	// VIP give Kevlar+Helmet
	if(Client_HasAdminFlags(iClient, ADMFLAG_RESERVATION))
	{
		if(g_cvKevlarVIP.IntValue > 0)
			SetEntProp(iClient, Prop_Data, "m_ArmorValue", 100);
		if(g_cvKevlarVIP.IntValue == 2)
			SetEntData(iClient, g_iHelmetOffset, 1);
	}
	
	// Give Grenades
	GiveGrenades(iClient);
	
	// Switch to other weapons
	
	if(weapon > 0)
	{
		EquipPlayerWeapon(iClient, weapon);
		ClientCommand(iClient, "slot%d", CS_SLOT_SECONDARY+1);
		ClientCommand(iClient, "slot%d", CS_SLOT_PRIMARY+1);
	}
}

int GiveMP(int iClient)
{
	int iChance = GetRandomInt(1, 100) + iClient == g_iBomber ? 30 : 0;
	
	if(iChance > 95)
		return GivePlayerItem(iClient, "weapon_p90");
		
	if(iChance > 80)
		return GivePlayerItem(iClient, "weapon_ssg08");
	
	if(iChance > 60)
		return GivePlayerItem(iClient, "weapon_ump45");
	
	if(iChance > 40)
		return GivePlayerItem(iClient, "weapon_mp7");
	
	if(GetClientTeam(iClient) == CS_TEAM_CT)
		return GivePlayerItem(iClient, "weapon_mp9");
	
	return GivePlayerItem(iClient, "weapon_mac10");
}

int GiveShotgun(int iClient)
{
	int iChance = GetRandomInt(1, 100) + iClient == g_iBomber ? 35 : 0;
	
	if(iChance > 90)
		return GivePlayerItem(iClient, "weapon_mag7");
	
	if(iChance > 45)
		return GivePlayerItem(iClient, "weapon_sawedoff");
	
	return GivePlayerItem(iClient, "weapon_nova");
}

int GivePrimaryWeapon(int iClient, int iTeam)
{
	if(StrEqual(gs_WantPrimary[iClient], "m4a1"))
	{
		if(iTeam == CS_TEAM_CT)
			return GivePlayerItem(iClient, "weapon_m4a1_silencer");
		return GivePlayerItem(iClient, "weapon_ak47");
	}
	else if(StrEqual(gs_WantPrimary[iClient], "famas"))
	{
		if(iTeam == CS_TEAM_CT)
			return GivePlayerItem(iClient, "weapon_famas");
		return GivePlayerItem(iClient, "weapon_galilar");
	}
	else if(StrEqual(gs_WantPrimary[iClient], "aug"))
	{
		if(iTeam == CS_TEAM_CT)
			return GivePlayerItem(iClient, "weapon_aug");
		return GivePlayerItem(iClient, "weapon_sg556");
	}
	else if(StrEqual(gs_WantPrimary[iClient], "ssg"))
	{
		return GivePlayerItem(iClient, "weapon_ssg08");
	}
	else
	{
		if(iTeam == CS_TEAM_CT)
			return GivePlayerItem(iClient, "weapon_m4a1");
		return GivePlayerItem(iClient, "weapon_ak47");
	}
}

int GiveDefaultPistol(int iClient, int iTeam)
{
	if(iTeam == CS_TEAM_CT)
		return GivePlayerItem(iClient, "weapon_hkp2000");
	
	return GivePlayerItem(iClient, "weapon_glock");
}

int GivePrefencePistol(int iClient, int iTeam)
{
	if(g_iRoundType != RT_FORCE && g_iPistolPrefence[iClient] == 0)
	{
		if(iTeam == CS_TEAM_CT)
			return GivePlayerItem(iClient, "weapon_hkp2000");
		
		return GivePlayerItem(iClient, "weapon_glock");
	}
	
	if(g_iPistolPrefence[iClient] == 1)
	{
		if(GetRandomInt(1, 100) <= g_cvStrongPistolChance.IntValue)
		{
			if(iTeam == CS_TEAM_CT)
				return GivePlayerItem(iClient, "weapon_fiveseven");
			
			return GivePlayerItem(iClient, "weapon_tec9");
		}
		
		return GivePlayerItem(iClient, "weapon_p250");
	}
	
	if(g_iPistolPrefence[iClient] == 3)
	{
		if(GetRandomInt(1, 100) <= g_cvDeaglePistolChance.IntValue)
			return GivePlayerItem(iClient, "weapon_deagle");
		
		if(GetRandomInt(1, 100) <= g_cvStrongPistolChance.IntValue)
		{
			if(iTeam == CS_TEAM_CT)
				return GivePlayerItem(iClient, "weapon_fiveseven");
			
			return GivePlayerItem(iClient, "weapon_tec9");
		}
		
		return GivePlayerItem(iClient, "weapon_p250");
	}
	
	if(g_iPistolPrefence[iClient] == 4)
	{
		if(GetRandomInt(1, 100) <= g_cvDeaglePistolChance.IntValue)
			return GivePlayerItem(iClient, "weapon_deagle");
		
		if(GetRandomInt(1, 100) <= g_cvStrongPistolChance.IntValue)
			return GivePlayerItem(iClient, "weapon_cz75a");
		
		return GivePlayerItem(iClient, "weapon_p250");
	}
	
	// 2 or 0
	return GivePlayerItem(iClient, "weapon_p250");
}

int GetRandomAwpPlayer(int iTeam)
{
	Handle aPlayers = CreateArray(1);
	LoopIngameClients(i)
	{
		if(GetClientTeam(i) != iTeam)
			continue;
		
		PushArrayCell(aPlayers, i);
	}
	
	if(g_cvAwpPrioVIP.IntValue == 2)
		SortADTArrayCustom(aPlayers, SortPlayersByPointsDescButVip);
	else if(g_cvAwpPrioVIP.IntValue == 1)
		SortADTArrayCustom(aPlayers, SortPlayersByPointsDesc);
	else SortADTArray(aPlayers, Sort_Random, Sort_Integer);
	
	int iAWP = -1;
	
	LoopArray(iIndex, aPlayers)
	{
		int iTarget = GetArrayCell(aPlayers, iIndex);
		
		// Player likes to get the awp always
		if(g_iWantAwp[iTarget] == 1)
		{
			if(g_cvDebug.BoolValue)
				LogMessage("[RETAKE-AWP] Team: %d AWP: %N", iTeam, iTarget);
			iAWP = iTarget;
			break;
		}
		
		// Dice if the player likes to get the awp not always
		if(GetRandomInt(0, 2) == 0)
		{
			if(g_cvDebug.BoolValue)
				LogMessage("[RETAKE-AWP] Team: %d AWP: %N (By chance)", iTeam, iTarget);
			iAWP = iTarget;
			break;
		}
		
		if(g_cvDebug.BoolValue)
			LogMessage("[RETAKE-AWP] Team: %d Chance failed for %N", iTeam, iTarget);
	}
	
	return iAWP;
}